\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT247 Notes: Eigenvectors, Eigenvalues and Diagonalizability}
\author{Jad Elkhaleq Ghalayini}
\date{January 11 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}

\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem*{corollary}{Corollary}
\newtheorem*{fact}{Fact}
\newtheorem*{note}{Note}

\usepackage{tikz}

\begin{document}

\maketitle

Recall: \(A \in Mat_{n \times n}(F)\) has characteristic polynomial
\[P_A(t) = \det(A - t\cdot I_n) = \ \text{polynomial in} \ t\]
When we take the determinant of this, we sum the products of all possible permutations times the sign of the permutation. We claim that among all the different products, there's one that looks like this
\[(a_{11} - t)(a_{22} - t)...(a_{nn} - t) + \ \text{other} \ = (-1)^nt^n + \ \text{other}\]
In the other terms, we've omitted at least one diagonal, and hence the degree is at most \(n - 1\). So we see overall we have a polynomial in \(t\) of degree \(n\) with a top term of \((-1)^nt^n\). If we want to know the constant term \(c\) at the end of a polynomial \((-1)t^{n} + ... + c\), we just set \(t = 0\). So we get
\[c = P_A(0) = \det(A)\]
All the other coefficients are nice, important things attached to \(A\) as well.
By the way, we can state the abstract version.
\begin{definition}
Suppose we have \(T: V \to V\), \(\dim V = n\). If \(\beta\) is a basis for \(V\)
\[P_T(t) = \det([T]_\beta - t\cdot I_n) = P_{[T]_\beta}(t)\]
\end{definition}
We'll stop doing this separately soon. There is some content, however: why doesn't this depend on the basis. We have to show this is well defined (i.e. independent of the choice of \(\beta\)).

Suppose we choose bases \(\beta, \gamma\), giving matrices \([T]_\gamma = Q^{-1}[T]_\beta Q\), since we know the two matrices are similar. So let's look at
\[\det([T]_\gamma - t \cdot I_n) = \det(Q^{-1}[T]_\beta Q - t \cdot I_n) = \det(Q^{-1}[T]_\beta Q - t \cdot Q^{-1}I_nQ)\]
\[= \det(Q^{-1}([T]_\beta - t \cdot I_n)Q) = \det(Q)^{-1}\det(Q)\det([T]_\beta - t \cdot I_n) = \det([T]_\beta - t \cdot I_n)\]
So \(P_T\) is well defined as the choice of \(\beta\) does not matter, since
\[P_T = P_{[T]_\beta} = P_{[T]_\gamma}\]
As we proved last time,
\begin{theorem}
\begin{enumerate}
    \item The eigenvalues of \(A\) are the same as the roots of the characteristic polynomial \(P_A\)
    \item If \(\lambda\) is an eigenvalue of \(A\), then \(\{0\} \neq N(A - \lambda I_n)\) and any \underline{non-zero} vector \(x \in N(A - \lambda I_n)\) is an \underline{eigenvector} of \(A\).
    \begin{definition}
        \(E_\lambda = N(A - \lambda I_n)\) is the \(\lambda\) \underline{eigenspace} of \(A\), which is a subspace of \(V\).
    \end{definition}
\end{enumerate}
\end{theorem}
\begin{corollary}
\(A\) has at \underline{most} \underline{\(n\) distinct} eigenvalues.
\end{corollary}
\begin{proof}
A polynomial \(P_A(t)\) of degree \(n\) has at most \(n\) roots.
\end{proof}
\begin{note}
\(\alpha\) is a root of \(f(t)\) if and only if \(f(t) = (t - \alpha)g(t)\). Make sure to review polynomial long division!
\end{note}
\begin{theorem}
\(A\) is diagonalizable \(\iff \ \exists\) a basis of eigenvectors.
\end{theorem}
Note the same applies to \(T\).
\begin{note}
Suppose \(V = C^{\infty}(\mathbb{R})\) (the infinitely differentiable functions on the real line).
\[T: V \to V, f \mapsto f'\]
\[T(f) = \lambda f \implies f' = \lambda f \implies f = c \cdot e^{\lambda t}\]
So \(c \cdot e^{\lambda t}\) is an \underline{eigenvector} assuming \(c \neq 0\), the \underline{eigenfunction of the differential operator}.
Notice \(\lambda\) can be any real number, i.e. there are uncountably infinite eigenvalues! Usually, for such spaces, linear algebra must be combined with \underline{serious analysis}.
\end{note}
Examples
\begin{enumerate}

\item \(n = 2\),
\[A = \begin{pmatrix} a & b \\ c & d \end{pmatrix} \in Mat_{n \times n}(\mathbb{R}) \implies P_A(t) = t^2 - (a + d)t + ad - bc\]
\[= t^2 - tr(A)t + det(A)\]
We can use the quadratic formula to get roots \(\alpha_1, \alpha_2 \in \mathbb{C}\), from the factorization
\[P_A(t) = (t - \alpha_1)(t - \alpha_2)\]
If the roots are not in \(\mathbb{R}\), then \(\alpha_2 = \bar{\alpha_1}\).

\item Last time:
\[A = \begin{pmatrix} 3/2 & -1/4 \\ 1 & 1/2\end{pmatrix}, P_A(t) = (t - 1)^2\]
\[\lambda = 1, A - \lambda I_2 = \begin{pmatrix} 1/2 & - 1/4 \\ 1 & -1/2\end{pmatrix}\begin{pmatrix}1 \\ 2\end{pmatrix} = 0\]
Claim: \(E_1 = \mathbb{R} \cdot \begin{pmatrix} 1 \\ 2 \end{pmatrix}\)
By dimension, either \(E_1 = R \cdot \begin{pmatrix} 1 \\ 2\end{pmatrix}\) or \(E_1 = \mathbb{R}^2\). But only the zero matrix sends every vector to 0, so the second choice cannot be true.

\item \(A = I_2 \implies P_A(t) = (t - 1)^2\), but \[A - 1I_2 = I_2 - I_2 = 0_2\]
so \(N(A - I_2) = N(0_2) = \mathbb{R}^2\).

\item Consider 
\[A = \begin{pmatrix}0 & 1 \\ -1 & 0\end{pmatrix} \implies P_A(t)  = (t - i)(t + i)\]
However, this matrix, thought of as a real matrix, has no eigenvalues in \(\mathbb{R}\). So this guy is not diagonalizable \textit{in the field in question}. However, if we decide to work over \(\mathbb{C}\), taking \(A \in Mat_{2 \times 2}(\mathbb{C})\), we have eigenvalues \(\pm i\) in \(\mathbb{C}\).
Solving for \(\lambda = 1\),
\[E_i = \mathbb{C} \cdot \begin{pmatrix} 1 \\ i \end{pmatrix}\]
If
\[Ax = \lambda x, \bar{Ax} = A\bar{x} = \bar{\lambda}\bar{x}\]
For real values where \(\bar{r} = r\), we gain nothing, but here, we quickly find that
\[E_{-i} = \mathbb{C} \cdot \begin{pmatrix} 1 \\ -i \end{pmatrix}\]

\end{enumerate}

\begin{theorem}
\(T: V \to V \in \mathcal{L}(V), \dim V = n\) and let \(\lambda_1,...,\lambda_k\) be \(k \leq n\) distinct eigenvalues of \(T\) and
\(v_1,...,v_k\) \underline{eigenvectors} such that \(T(v_j) = \lambda_jv_j\). Then \(\{v_1,...,v_k\}\) is linearly independent (LI).
\end{theorem}
\begin{corollary}
If \(T\) has \(n\) distinct eigenvalues, then \(T\) is diagonalizable.
\end{corollary}
\begin{note}
The characteristic polynomial of an upper or lower triangular \(n \times n\) matrix \(A\) is given by
\[P_A = \prod_{i = 0}^n(A_{ii} - t)\]
giving eigenvalues \(A_{11},...,A_{nn}\).
\end{note}
We now prove the theorem:
\begin{proof}
Induction on \(k\):
\begin{itemize}
    \item [\(k = 1\):] \(\lambda_1, v_1\), \(\{v_1\}\) is linearly independent \(\iff v_1 \neq 0\). By definition, \(v_1 \neq 0\) as an eigenvector
    \item [General case:] Assume
    \[\sum_{i = 0}^ka_iv_i = 0\]
    We're going to apply the linear transformation \(T - \lambda_k I\) to both sides of the equation to get
    \[(T - \lambda_k I)\sum_{i = 0}^ka_iv_i = \sum_{i = 0}^ka_i(\lambda_iv_i - \lambda_kv_i) = \sum_{i = 0}^{k - 1}a_i(\lambda_iv_i - \lambda_kv_i) = 0\]
    Since \(a_k(\lambda_k - \lambda_k)v_k = 0\).
    By induction, we know that \(a_1(\lambda_1 - \lambda_k) = 0, a_2(\lambda_2 - \lambda_k) = 0, ..., a_{k - 1}(\lambda_{k - 1} - \lambda_{k}) = 0\).
    Since \(\lambda_1,...,\lambda_k\) are distinct, \((\lambda_1 - \lambda_k),...,(\lambda_{k - 1} - \lambda_k) \neq 0\), so \(a_1,...,a_{k - 1} = 0\).
    So \[\sum_{i = 0}^ka_iv_i = a_kv_k = 0 \implies a_k = 0\] since \(v_k \neq 0\) as an eigenvector.
\end{itemize}
\end{proof}

\begin{definition}
Say \(f(t)\) \underline{splits completely} (SC) if and only if \(f(t) = c(t - \alpha_1)...(t - \alpha_n), n = deg(f)\), \(\alpha_i \in F\). So, for example, a polynomial can split completely in \(\mathbb{C}\) (all complex/real polynomials do) but not in \(\mathbb{R}\).
\end{definition}
\begin{note}
If \(T: V \to V\) is diagonalizable, then \(P_T(t)\) SC.
\end{note}
Because I take \(\beta\) such that \([T]_\beta = diagonal(\lambda_1,...,\lambda_n) \implies P_T(t) = (\lambda_1 - t)...(\lambda_n - t)\)
\begin{definition}
Let \(f(t)\) be a polynomial having root \(\alpha\).
\[f(t) = (t - \alpha)^{m_\alpha}g(t), g(\alpha) \neq 0\] implies \(\alpha\) has multiplicity \(m_\alpha \geq 1\).
\end{definition}
\begin{note}
If \(T\) is diagonalizable, and \(\lambda\) is an eigenvalue, then \(\dim E_\lambda = m_\lambda\).
\end{note}
\begin{proof}
\[\dim N(T - \lambda I) = \dim N([T]_\beta - \lambda I_n)\]
Take \(\beta\) such that
\[[T]_\beta = diagonal(\lambda, \lambda,..., \mu_1, ..., \mu_k)\]\[ \implies P_T(t) = (-1)^n(1 - \lambda)^{m_\lambda}(t - \mu_1)...(t - \mu_k)\]
\[[T]_\beta - \lambda \cdot I_n = diag(0, 0,..., \mu_1 - \lambda, ..., \mu_k - \lambda)\] (with \(\mu_1 - \lambda,...,\mu_k - \lambda \neq 0\)).
Multiplying \([T]_\beta - \lambda \cdot I_n\) by any vector of the form
\[\begin{pmatrix}b_1 \\ \vdots \\ b_{m_\lambda} \\ 0 \\ \vdots \\ 0\end{pmatrix}\]
gives 0 otherwise, the value is always non-zero (since a diagonal matrix just multiplies rows of a vector), this subspace having dimension \(m_\lambda\). So
\[\dim N([T]_\beta - \lambda I_n) = m_\lambda\]
\end{proof}
\begin{theorem}
Let \(T: V \to V \in \mathcal{L}(V), \dim V = n\), and let \(\lambda\) be an eigenvalue of \(T\) with multiplicity \(m_\lambda\). Then
\[1 \leq \dim E_\lambda \leq m_\lambda\]
\end{theorem}
\begin{theorem}
\begin{enumerate}
    \item \(T\) is diagonalizable \(\iff \dim E_\lambda = m_\lambda \ \forall \ \lambda\)
    \item If \(T\) is diagonalizable, \(\lambda_1,...,\lambda_n\) are the eigenvalues and \(E_{\lambda_1},...,E_{\lambda_n}\) are the corresponding eigenspaces, suppose
    \(\beta^1,...,\beta^n\) are bases for \(E_{\lambda_1},...,E_{\lambda_2}\).
    Then \[\beta = \beta^1 \cup \beta^2 \cup ... \cup \beta^n\] is a basis for \(V\).
\end{enumerate}
\end{theorem}
We'll prove these things next time.
\end{document}