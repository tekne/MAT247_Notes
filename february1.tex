\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT247 Notes: Orthonormal Basis}
\author{Jad Elkhaleq Ghalayini}
\date{February 1 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{cancel}

\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{corollary}{Corollary}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{note}{Note}
\newtheorem*{goal}{Goal}

\newcommand{\defeq}[0]{\vcentcolon=}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}

\usepackage{tikz}

\begin{document}

\maketitle

Let's catch up where we left off last time: the inner product \(\ip{}{}: V \times V \to F (= \reals, \comps)\) has the following axioms
\begin{enumerate}

    \item The inner product is \textit{sesquilinear} (on the real numbers, its simply):
    \[\ip{\alpha x + \beta y}{z} = \alpha\ip{x}{z} + \beta\ip{y}{z}\]
    \[\ip{x}{\alpha y + \beta z} = \overline{\alpha}\ip{x}{y} + \overline{\beta}\ip{x}{z}\]
    
    \item Symmetric over \(\reals\), or Hermitian over \(\comps\):
    \[\ip{x}{y} = \overline{\ip{y}{x}}\]
    
    \item Positive definite (in the complex case, \underline{real} and positive):
    \[\ip{x}{x} > 0 \ \forall \ x \neq 0\]
    
\end{enumerate}
Mathematics doesn't just come out of thin air: we don't just write down axioms to bother people. The axioms above have a long history, which we compressed into a blackboard. We'll quickly go through some of it
\begin{itemize}
    \item We start with a generalization of vectors in the plane: which can be added, subtracted and multiplied
    \item After axiomatizing these aspects of plane geometry, we realized these axioms have other interesting consequences, and can be applied to many other things
    \item But the axiomatization of a vector space is missing two things: angle and length. Quite a lot of plane geometry, however, is about length and angles. You can't even say the word ``circle'' without knowing about length. Then you find that if you have two vectors \(x, y\), and write them as \((x_1,x_2), (y_1,y_2)\), then all the length and angle information can be encoded into a simple thing: the inner product.
    \item So we write
    \[\ip{(x_1, x_2)}{(y_1, y_2)} = x_1y_1 + x_2y_2 = ||x||||y||\cos\theta\]
    (try proving this). So
    \[\ip{x}{x} = ||x||^2\cos(0) = ||x||^2\]
    So if you know inner produces, you know norms. But if you know both inner products and norms, you know cosines, and hence angles.
    \item You go and stare at this expression over time, and find that it has simple properties (the inner product space axioms), and find that everything you would want to prove about it come from these properties. So we promote the properties of this example to become the axioms in the definition of something more general. And now, if we find other examples, anything we prove will be true in the other examples as well
    \item There's a little catch though: to make things work over the complex numbers, you need to add conjugates everywhere to make things work.
\end{itemize}
Examples
\begin{enumerate}
    \item \[\ip{\begin{pmatrix}a_1 \\ \vdots \\ a_n\end{pmatrix}}{\begin{pmatrix}b_1 \\ \vdots \\ b_n\end{pmatrix}} = \sum_{i = 1}^na_i\bar{b_i}\]
    \item \[\ip{f}{g} = \int_0^{2\pi}f(x)\overline{g(x)}dx, f, g:[0, 2\pi] \to \comps\]
    \item \[\ip{A}{B} = B*A, A, B \in M_{n \times n}(\comps)\] where \(B*\) is the conjugate transpose
\end{enumerate}
We can prove some general properties about inner products
\begin{itemize}
    \item [Cauchy Schwartz: ] \(|\ip{x}{y}| \leq ||x||||y||\)
    \item [Triangle:] \(||x + y|| \leq ||x|| + ||y||\)
\end{itemize}
\begin{definition}
\(x \perp y\): ``\(x\) is perpendicular to \(y\)''
\[\iff \ip{x}{y} = 0\]
\end{definition}
Minor point: \(0 \perp x \ \forall \ x \in V\).
\begin{claim}
If \(x \perp y\), then Pythagoras applies, e.g.
\[||x + y||^2 = ||x||^2 + ||y||^2\]
\end{claim}
\begin{proof}
\[\ip{x + y}{x + y} = \ip{x}{x} + \ip{y}{y} + \ip{x}{y} + \ip{y}{x} = ||x||^2 + ||y||^2 + 0 + \bar{0} = ||x||^2 + ||y||^2\]
\end{proof}
\begin{definition}
An orthogonal set \(S \subset V\) means
\[\forall \ x, y \in S, x \neq y \implies x \perp y\]
\end{definition}
\begin{definition}
An orthnormal set \(S \subseteq V\) is an orthogonal set where
\[\forall \ x \in S, ||x|| = 1\]
e.g. in the countably infinite case, \(S = \{v_1,...\}\),
\[\ip{v_i}{v_j} = \left\{\begin{array}{c}1 \text{ if } i = j \\ 0 \text{ if } i \neq j\end{array}\right. = \delta_{ij}\]
\end{definition}
Now for the big thing from last time, 
\subsection*{Gram-Schmidt Orthonormalization}
Given \(S = \{v_1,...,v_n\}\) which are linearly independent in \(V\) (we're always assuming an inner product is given here), there exists an orthonormal subset
\[S' = \{w_1,...,w_n\} \subset V\]
which is orthonormal such that \(\forall j \leq k, span\{v_1,...,v_j\} = span\{w_1,...,w_j\}\)
\begin{theorem}
Every finite dimensional inner product space \(B\) has an orthonormal basis. If \(\beta = \{v_1,...,v_n\}\) is such a basis, then
\[\forall x \in V, x = \sum\alpha_iv_i, \alpha_i = \ip{x}{v_i}\]
These \(\alpha_i\)'s are called the ``Fourier coefficients of \(x\)''.
\end{theorem}
We'll do two examples: one trivial, and one deep.
\begin{enumerate}
    \item \(V = \reals^2\), standard inner product. Let \(\beta = \{e_1,...,e_n\}\) be the standard basis. It is very easy to see that this basis is orthonormal.
    \[x = \begin{pmatrix}x_1 \\ \vdots \\ x_n\end{pmatrix}, \alpha_i = \ip{x}{e_i} = x_i\]
    \[\implies x = \sum x_ie_i\]
    \item Let \(V = C([0, 2\pi] \to \comps)\) (the set of continuous functions defined on this interval with complex values). This is clearly a vector space over \(\comps\) via complex addition. Let
    \[\ip{f}{g} = \frac{1}{2\pi}\int_0^{2\pi}f(x)\bar{g(x)}dx\]
    \underline{Claim}:
    \[\{v_k(x) \defeq e^{ikx}\}_{k = -\infty}^{k = \infty}\] is orthonormal
    
    ``\underline{Almost truth}'': if \(f \in V\),
    \[f(x) = \sum_{k = -\infty}^\infty\alpha_k\cdot e^{ikx}\]
    where
    \[\alpha_k = \ip{f}{v_k} = \frac{1}{2\pi}\ip_0^{2\pi}f(x)e^{-ikx}dx\]
    As written this is FALSE, since in general we cannot add infinitely many things. To add infinitely many things, we need to go to places where we talk about convergence like MAT157, MAT257. In other classes, these convergence issues are resolved, and it becomes a true theorem, with some small print.
    
    Regardless, this is the beginning of Fourier analysis: that every function can be written as a linear combination of pure waves.
\end{enumerate}
Let's now return to solid mathematics
\begin{claim}
In the setup for Gram-Schmidt orthogonalization, suppose
\[x = \sum\alpha_iv_i, y = \sum\beta_iv_i\]
Then it is very easy to compute
\[\ip{x}{y}\]
As we complete the proof, we'll also write down what the inner product is
\end{claim}
\begin{proof}
\[\ip{x}{y} = \ip{\sum_{i = 1}^k\alpha_iv_i}{\sum_{j = 1}^k\beta_jv_j}\]
By sesquilinearity, we can write
\[\ip{x}{y} = \sum_{i = 1}^k\alpha_i\sum_{j = 1}^k\bar{\beta_j}\ip{v_i}{v_j}\]
Since this is an orthonormal set, \(\ip{v_i}{v_j} \neq 0 \iff i = j \implies \ip{v_i}{v_j} = 1\). So
\[\ip{x}{y} = \sum_{i = 1}^n\alpha_i\bar{\beta}_i\]
\end{proof}
Moral: at some point you might have learned that all finite dimensional vector spaces are isomorphic to \(F^n\). Now, we have a stronger moral: ``all finite dimensional inner product spaces are the same as \(\comps^n\) with the standard inner product.'' 

So why bother studying other inner products? Because whilst every canonical vector space is isomorphic to \(F^n\), this is only after choosing a basis. But people might think of different bases! For example, there are many ways to write polynomials in terms of \(F^n\), etc.

\begin{corollary}
In the above setup, suppose in addition you are given \(T: V \to V\), being a linear transformation, and suppose \(A = [T]_\beta\) for an orthonormal basis \(\beta\). Then
\[A_{ij} = \ip{Tv_j}{v_i}\]
\end{corollary}
\begin{proof}
To make the matrix \(A\), for each row \(j\), we must compute \(Tv_j\) and write it in terms of basis vectors. But we have a formula to  find the coefficients of \(Tv_j\), and any vector \(v\) for that matter, in terms of \(\{v_1,...,v_k\}\):
\[v = \sum_i\ip{v}{v_i}\cdot v_i\]
\end{proof}

\begin{definition}
If \(S \subset V\), then
\[S^\perp = \{x \in V:\forall y \in S, x \perp y\}\] 
This is called the orthogonal or perpendicular complement
\end{definition}
Examples
\begin{enumerate}
    \item \(\varnothing^\perp = V\)
    \item \(\{0\}^\perp = V\)
    \item \(V^\perp = \{0\}\)
    \begin{proof}
    It's clear that \(0 \in V^\perp\). Suppose now \[x \in V^\perp \implies x \perp x \implies \ip{x}{x} = 0 \implies x = 0\]
    So \(x \in V^\perp \iff x = 0 \implies V^\perp = \{0\}\)
    \end{proof}
\end{enumerate}
\begin{claim}
No matter what \(S\) is, \(S^\perp\) is a subspace.
\end{claim}
\begin{proof}
Suppose \(x, y \in S^\perp, \alpha, \beta \in F\)
\[\forall z \in S, \ip{\alpha x + \alpha y}{z} = \alpha\ip{x}{z} + \beta\ip{y}{z} = 0 + 0 = 0\]
\[\implies \alpha x + \beta y \in S^\perp\]
\end{proof}
\begin{claim}
\(S^\perp \cap S \subset \{0\}\)
\end{claim}
\begin{proof}
Suppose \(x \in S \cap S^\perp\). Then \(x \in S^\perp, S \implies x \perp x \implies x = 0\)
\end{proof}
\begin{theorem}
Given a subspace \(W \subset V\) and \(y \in V\),
\[\exists! u\in W, z \in W^\perp, y = u + z\]
\end{theorem}
Note: \(\exists!\) means exists a unique.
This theorem is very hard to prove without a hint. But with the hint, it becomes very easy:
In fact, if \(v_1,...,v_k\) is an orthonormal basis of \(W\), then
\[u = \sum_{i = 1}^k\ip{y}{v_i}\cdot v_i, z = y - u\]
The regrettable thing about claims and theorems is that one has to prove them.
\begin{proof}
We know \(y = u + z, u \in W\). So we need to prove
\begin{enumerate}
    \item \(z \in W^\perp\):
    Compute
    \[\ip{z}{v_j} = \ip{y - u}{v_j} = \ip{y}{v_j} - \ip{\sum_{i = 1}^k\ip{y}{v_i}v_i}{v_j}\]
    \[= \ip{y}{v_j} - \sum_{i = 1}^k\ip{y}{v_i}\ip{v_i}{v_j} = \ip{y}{v_j} - \ip{y}{v_j} = 0\]
    \underline{Aside}: \(z \perp x, z \perp y \implies z \perp (\alpha x + \beta y)\)
    So we conclude that \(z\) is perpendicular to all linear combinations of \(v_j\)'s, and hence to all elements in \(W\). So \(z \in W^T\)
    \item \(u, z\) are unique.
    \[\{book, Kudla\} \implies uniqueness\]
\end{enumerate}
\end{proof}
\end{document}