\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT247 Notes: Self-adjoint operators}
\author{Jad Elkhaleq Ghalayini}
\date{March 1 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}

\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{corollary}{Corollary}
\newtheorem*{fact}{Fact}
\newtheorem*{note}{Note}
\newtheorem*{claim}{Claim}

\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}


\newcommand{\defeq}[0]{\vcentcolon=}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\comps}[0]{\mathbb{C}}

\usepackage{tikz}

\begin{document}

\begin{theorem}
If \(V\) is a finite dimensional inner product space over \(\comps\), \(T\) is normal if and only if there exists an orthonormal basis \(\beta\) of eigenvectors such that
\[[T]_\beta = \underline{diagonal}\]
\end{theorem}
\begin{proof}
\(P_T(t) = \text{ \underline{splits}}\)
\[= (-1)^n(t - \lambda_1)...(t - \lambda_N) / \comps\]
Thus, by Schur's theorem, \(\exists\) an orthonormal basis \(\beta\) such that
\[[T]_\beta = \text{ \underline{upper triangular}}\]
\begin{claim}
\(X\) is upper triangular and \(XX^* = X^*X \implies X = \) diagonal
\end{claim}
\begin{proof}
This proof is a substitute for the argument in the book, using induction on the size of the matrix \(n\):
    \begin{itemize}
    
        \item Base case (\(n = 1\)):
        
        \item Induction step:
            \[\begin{pmatrix} A & B\\ 0 & C \end{pmatrix}\begin{pmatrix} A^* & 0 \\ B^* & C^* \end{pmatrix} = \begin{pmatrix} AA^* BB^* & BC^* \\ CB^* & CC^* \end{pmatrix}\]
            \[\begin{pmatrix} A^* & 0 \\ B^* & C^* \end{pmatrix}\begin{pmatrix} A & B\\ 0 & C \end{pmatrix} = \begin{pmatrix} A^*A & A^*B \\ B^*A & B^*B + C^*C \end{pmatrix}\]
            If \(A\) is \((x)\),
            \[A^*A = AA^* \implies BB^* = 0\]
            Now, consider the associated
            \[B = \begin{pmatrix}b_1 & ... & b_{n - 1}\end{pmatrix}\]
            We have
            \[BB^* = \begin{pmatrix} b_1 & ... & b_{n - 1} \end{pmatrix}\begin{pmatrix} \bar{b}_1 \\ \vdots \\ \bar{b}_{n - 1}\end{pmatrix} = |b_1|^2 + ... + |b_{n - 1}|^2 = 0\]
            implying that \(B = 0\). Hence, we can write
            \[X = \begin{pmatrix} A & 0 \\ 0 & C \end{pmatrix} \implies C^*C = CC^*\]
            where \(C \in Mat_{n - 1 \times n - 1}(\comps)\). Hence, applying the inductive hypothesis, \(C\) is diagonal implying \(X\) is diagonal.
            
    \end{itemize}
\end{proof}


\end{proof}

\begin{itemize}

    \item [Warning:] consider
    \[H = C^\infty([0, 2\pi]), f_n(t) = e^{int}, H \supset V = span\{f_n | n \in \mathbb{Z}\}\]
    We know
    \[\ip{f}{g} = \frac{1}{2\pi}\int_0^2\pi f(t)\overline{g(t)}dt \implies \ip{f_n}{f_m} = \delta_{mn}\]
    Consider
    \[T: f \mapsto f_1 \cdot f \implies f_n \mapsto f_{n + 1}\]
    We claim that
    \[T^*: f \mapsto f_{-1} \cdot f\]
    You can check this claim. You can furthemore check that \(T\) is normal, since
    \[TT^* = T^*T = I\]
    \underline{But} there are \underline{no} eigenvectors. Consider if there were an eigenvector
    \[f = \sum_ja_jf_j \in V\]
    Then we would have
    \[\sum_{j}\lambda_ja_jf_j = \lambda f = T(f) = \sum_ja_jf_{j + 1}\]
    Putting all the terms on one side, I have a linear relation between the vectors
    \[0 = \sum_{j}(a_{j - 1} - \lambda a_{j})f_j\]
    but the \(f_j\)'s are linearly independent, so
    \[a_{j - 1} - \lambda a_{j} = 0 \forall j\]
    This could certainly happen, but somewhere down at the bottom, there's a last nonzero \(a_j\), for which \(a_{j - 1} = 0 \implies \lambda a_j = 0\). But then either \(\lambda = 0\), or if \(\lambda \neq 0\), the next \(a_{j - 1}\) would have to be zero, and so on. So all \(a_j\) are zero, hence \(f\) is not an eigenvector, leading to a contradiction.
    
    \item [Recall:]
    \[A = \begin{pmatrix} 0 & 1 \\ -1 & 0 \end{pmatrix}, A^* = \begin{pmatrix} 0 & -1 \\ 1 & 0 \end{pmatrix} = -A\]
    \(A\) is \underline{normal} but over \(\reals\) has \underline{no eigenvectors}, i.e. \(\comps\) is necessary
    
\end{itemize}
\begin{definition}
\(T\) is self adjoint if \(T^* = T\) (\(\implies\) normal, trivially)
\end{definition}
\begin{theorem}
Let \(V\) be a finite dimensional inner product space over \(\reals\), and let \(T: V \to V\). Then
\[T^* = T \iff \exists \text{ an orthonormal basis of eigenvectors}\]
\end{theorem}
\begin{proof}
\begin{lemma}
Let \(V\) be a finite dimensional inner product space (over either \(\reals\) or \(\comps\)), and let \(T: V \to V\) be self-adjoint. Then
\begin{enumerate}
    \item Every eigenvalue of \(T\) is real
    \item \(P_T(t)\) splits
\end{enumerate}
\end{lemma}
\begin{proof}
\begin{enumerate}

    \item \(T(x) = \lambda x\)
    for some \(x \neq 0\).  Hence,
    \[T = T^* \implies T^*x = \bar{\lambda}x = Tx \implies \lambda = \bar{\lambda} \iff \lambda \in \reals\]
    i.e.
    \[0 = ||(T - \lambda I)(x)|| = ||(T^* - \bar{\lambda}I)(x)||\]
    
    \item \begin{itemize}
        \item [\(/\comps\):] automatic
        \item [\(/\reals\):] Choose an orthonormal \(\beta\) for \(V\). 
        \[[T]_\beta = [T^*]_\beta = [T]_\beta^*\]
        i.e.
        \[T = T^* \iff A = [T]_\beta, A = A^*\]
        So consider \(A\) viewed as a complex matrix: we know it splits completely, such that
        \[P_A(t) = (-1)^n(t - \lambda_1)...(t - \lambda_n)\]
        We've just shown \(\lambda_1,...,\lambda_n \in \reals\). So hence \(A\) splits completely in \(\reals\) as well!
    \end{itemize}
    
\end{enumerate}
\end{proof}
By the Lemma, we know that \(P_T(t)\) splits, therefore, by Schur's theorem, there is an orthonormal basis \(\beta\) for \(V\) such that
\([T]_\beta\)
is upper triangular. 
\[T^*T \iff [T]_\beta^* = [T]_\beta = [T]_\beta^T\]
Since an upper triangular matrix is equal to it's own transpose \(\iff\) its diagonal, we have
\[[T]_\beta = \text{ diagonal}\]
Hence, we have an orthonormal basis of eigenvectors.
\end{proof}

\subsection*{Direct Sums}

Let \(V\) be a vector space and \(W_1, W_2\) be subspaces. We say
\[V = W_1 + W_2\]
if \(\forall v \in V\),
\[\exists w_1 \in W_1, w_2 \in W_2, v = w_1 + w_2\]
Example:
\[V = \reals^3, W_1 = xy\text{-plane}, W_2 = yz\text{-plane}\]
Clearly,
\[V = W_1 + W_2\]
Notice, in this particular case it's not a unique decomposition, since a vector along the \(y\) axis can be written as something in \(W_1\) plus \(0 \in W_2\) or something in \(W_2\) plus \(0 \in W_1\). We say
\[V = W_1 \oplus W_2\]
(the \underline{direct sum} of \(W_1, W_2\)) if
\[V = W_1 + W_2, W_1 \cap W_2 = \{0\}\]
Example:
\[V = \reals^3 = W_1 \oplus W_2, W_1 = \reals \cdot e_1 = x\text{-axis}, W_2 = yz\text{-plane}\]
If we have \(V = W_1 \oplus W_2\), then
\[\forall v \in V, v \neq 0 \implies !\exists w_1 \in W_1, w_2 \in W_2, v = w_1 + w_2\]
More generally, if \(V\) is a vector space and \(W_1,...,W_k\) are subspaces
\begin{enumerate}

    \item Say \(V = W_1 + ... + W_k\) if every \(x \in V\) can be written as \(x = x_1 + ... x_k\), \(x_j \in W_j\)
    
    \item Say \(V = W_1 \oplus ... \oplus W_2\) if (1) and \[W_i \cap \left(\sum_{j \neq i}W_j\right) = 0\]
    
\end{enumerate}
\begin{theorem}
The following are equivalent
\begin{enumerate}[label=(\alph*)]
    
    \item \(V = W_1 \oplus ... \oplus W_k\)
    
    \item \(V = W_1 + ... + W_k\) and if \(v_1 + ... + v_k = 0\) where \(v_j \in W_j\) then \(v_j = 0 \forall j\)
    
    \item \(V = W_1 + ... + W_k\) and the for the expression of any \(x = x_1 + ... +x_k\), the \(x_j\) are unique.
    
    \item If \(\gamma_j\) is a basis for \(W_j\), then
    \[\gamma_1 \cup ... \cup \gamma_k\]
    is a basis for \(V\).
    
    \item There exist bases \(\gamma_j\) for \(W_j\) for each \(j\) such that
    \[\gamma_1 \cup ... \cup \gamma_k\]
    is a basis for \(V\)
    
\end{enumerate}
\end{theorem}
\begin{proof}
\begin{itemize}
    
    \item [(a) \(\implies\) (b):] If \(v_1 + ... + v_k = 0\), for each \(j\), rewrite this as \(\sum_{i \neq j}v_i = -v_j\)
    i.e.
    \[\left(\sum_{i \neq j}W_i\right) \cap W_j = 0\]
    \item [(b) \(\implies\) (c):]
    Let
    \[x = x_1 + ... + x_k = x_1' + ... + x_k'\]
    \[0 = (x - x_1') + ... + (x_k - x_k')\]
    By (b), \(x_j - x_j' = 0 \forall j\)
    
    \item [(c) \(\implies\) (d)]
    \[V = \sum_jW_j \implies V = span\{\gamma_1 \cup ... \cup \gamma_k\}\]
    Suppose some finite combination of elements of \(\gamma_1 \cup ... \cup \gamma_k\) is 0. Then \(\forall j \exists v_j \in span\{\gamma_j\}\). Then
    \[0 = v_1 + v_2 + ... + v_k \implies v_j = 0 \ \forall j\]
    Since \(\gamma_j\) is a basis, all coefficients of \(v_j\) must also be zero. Hence, overall, \(\gamma_1 \cup ... \cup \gamma_k\) is a basis
    
\end{itemize}
\end{proof}

\end{document}