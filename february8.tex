\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT247 Notes: Orthonormal projections}
\author{Jad Elkhaleq Ghalayini}
\date{February 8 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}

\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{corollary}{Corollary}
\newtheorem*{fact}{Fact}
\newtheorem*{note}{Note}

\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}


\newcommand{\defeq}[0]{\vcentcolon=}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\comps}[0]{\mathbb{C}}

\usepackage{tikz}

\begin{document}

Recall the definition of the dual space \(V^*\), the space of linear transformations \(g: V \to F\).
Examples:
\begin{enumerate}

    \item \(g(x) = 0 \ \forall \ x \in V\)
    
    \item Suppose \(v_1,...,v_n\) form a basis for \(V\) so \(x = a_1v_1 + ... + a_nv_n\).  Consider n functions \(g_j\) such that \(g_j(x) = a_j\). Then
    \[g_1,...,g_n\] is a \underline{dual} basis.
    
    \item Let \(V, \ip{}{}\) be an IPS, \(v \in V\), \(g_v \in V^*\), \(g_v(x) = \ip{x}{v}\).
    \item If \(v_1,...,v_n\) is ano orthonormal basis, then
    \[g_{v_1},...,g_{v_n}\] is the dual basis.
    
\end{enumerate}

\begin{theorem}[Riesz Representation Theorem]
Suppose \(V, \ip{}{}\) is a finite dimensional IPS. Then for any \(g \in V^*\) there is a unique vector \(v \in V\) such that \(g = g_v\).
\end{theorem}
\begin{proof}
\(g\) is determined by its values in any basis. Take \(v_1,...,v_n\) as an orthonormal basis for \(V\),
\[x = \sum_{k = 1}^na_kv_k \implies g(x) = \sum_{k = 1}^na_kg(v_k) = \sum_{k = 1}^n\ip{x}{v_k}g(v_k) = \ip{x}{\sum_{k = 1}^n\overline{g(v_k)}v_1}\]
This is unique, as
\[\ip{x}{v} = \ip{x}{v'} \ \forall \ x \iff \ip{x}{v - v'} \ \forall \ x \iff v - v' = 0 \iff v = v'\]
\end{proof}
\begin{theorem}
Let \(V, \ip{}{}\) be a finite dimensional IPS, \(T: V \to V\), then the adjoint \(T^*: V \to V\) \underline{exists}
\end{theorem}
Recall: an adjoint of \(T\) is a linear transformation \(S: V \to V\) such that
\[\ip{T(x)}{y} = \ip{x}{S(y)}\]
If \(S\) exists it is unique. Call \(S = T^*\)
\begin{proof}
Note for any fixed \(y\), \(g_{y,T}: x \mapsto \ip{T(x)}{y}\) is a \underline{linear functional}. By the Riesz Representation Theorem (RRT), there is a \underline{unique} vector \(S(y) \in V\) such that 
\[g_{y,T}: x = \ip{x}{S(y)}\]
So we get a function \(S: V \to V\) such that \[\ip{T(x)}{y} = \ip{x}{S(y)} \ \forall \ x, y\]

Claim: \(S: V \to V, y \mapsto S(y)\) is a linear transformation.

\[\ip{x}{S(y_1 + y_2)} = \ip{T(x)}{y_1 + y_2} = \ip{T(x)}{y_1} + \ip{T(x)}{y_2}\]\[ = \ip{x}{S(y_1)} + \ip{x}{S(y_2)} = \ip{x}{S(y_1) + S(y_2)}\]
so the function is additive. Similarly,
\[\ip{x}{S(cy)} = \ip{T(x),cy} = \bar{c}\ip{T(x)}{y} = \bar{c}\ip{x}{S(y)} = \ip{x}{cS(y)}\]
So the function is linear, and hence the adjoint, concluding the proof.
\end{proof}
Example:
\[V = F^n, \ip{}{} = \text{ \underline{std.} ip }, x = \begin{pmatrix}x_1 \\ \vdots \\ x_n\end{pmatrix}\]
\[\ip{x}{y} = y^*x = \sum_{k = 1}^n\bar{y}_kx_k \implies y^* = \begin{pmatrix}\bar{y}_1 & ... & \bar{y}_n\end{pmatrix}\]
So if \(A \in M_{m \times n}(F)\), \(A^* = \overline{A^T}\).

Consider \(A \in M_{n \times n}(F)\).
\[L_A: F^n \to F^n, x \mapsto Ax\]
Let's calculate \(L_A^*\).
\[\ip{L_Ax}{y} = y^*\cdot Ax = (y^*A)x = (A^*y)^*x = \ip{x}{L_{A^*}y}\]
So we proved that
\[L_A^* = L_{A^*}\]

\begin{theorem}
Suppose \(V, \ip{}{}\) is a finite dimensional IPS, \(T: V \to V, T^*\) and \(\beta = \{v_1,...,v_n\}\) is an orthonormal basis for \(V\).
\[[T^*]_\beta = [T]_\beta^*\]
\end{theorem}
\begin{proof}
(Sketch:)
\[V \to F^n, x \mapsto [x]_\beta\]
\[\ip{}{} \to \ip{}{}_{\text{std}}\]
\[\ip{x}{y} = \ip{[x]_\beta}{[y]_\beta}_{\text{std}}\]
So
\(T^*\) is represented by \(L_{[T]_\beta}^* = L_{[T]_\beta^*}\) in the basis \(\beta\), and hence \([T^*]_\beta = [T]_\beta^*\).
(Check this)
\end{proof}
Note:
\begin{itemize}
    \item \((T + U)^* = T^* + U^*\)
    \item \((TU)^* = U^*T^*\)
\end{itemize}
Example:
\[T: \reals\to\reals, T\begin{pmatrix}a \\ b \\ c\end{pmatrix} = \begin{pmatrix} 3c + 2b \\ 2c - b \\ a + c \end{pmatrix}
= \begin{pmatrix} 3 & 2 & 0 \\ 0 & -1 & 2 \\ 1 & 0 & 1 \end{pmatrix}\begin{pmatrix}a \\ b \\ c\end{pmatrix}\]
So
\[T^*\begin{pmatrix}a \\ b \\ c\end{pmatrix} = \begin{pmatrix} 3 & 0 & 1 \\ 2 & -1 & 0 \\ 0 & 2 & 1 \end{pmatrix}\begin{pmatrix}a \\ b \\ c\end{pmatrix}
= \begin{pmatrix} 3a + c \\2a - b \\ 2b + c \end{pmatrix}\]
For a slightly slicker example, consider
\[V = C^\infty([0, 2\pi], \text{periodic})\]
\[\ip{f}{g} = \frac{1}{2\pi}\int_0^{2\pi}f(t)g(t)dt\]
Consider
\[T: V \to V, f \mapsto f' = Df, D = \frac{d}{dt}\]
\[\ip{T(f)}{g} = \frac{1}{2\pi}\int_0^{2\pi}f'(t)g(t)dt\]
Note that
\[D(fg) = Df\cdot g + f\cdot Dg\]
\[\int_0^{2\pi}(fg)'dt = \left.f \cdot g\right|_0^{2\pi} = f(2\pi)g(2\pi) - f(0)g(0) = 0\]
since the functions are periodic, and hence
\[\int_0^{2\pi}(fg)'dt  = \int_0^{2\pi}f'gdt + \int_0^{2\pi}fg'dt = 0 \implies \int_0^{2\pi}f'gdt = -\int_0^{2\pi}fg'dt\]
So
\[\ip{T(f)}{g} -\ip{f}{T(g)}\]
So
\[T^* = -T\]
implying differentiation is not quite self-adjoint, but rather minus or skew adjoint.

Consider \(A \in M_{m \times n}(F)\). We want to solve \(Ax = y, x \in F^m, y \in F^n\).

Let \(V = F^n, W = \{Ax \in V | x \in F^n\}\). 

What do we do when there is no solution \(y\). A story first: once upon a time there were search engines where if you misspelled a word, you would get nothing. But Google, on the other hand, helps you out. So what is Google doing? Trying to return the best possible solution, rather than that there is no solution.

So we want to find the closest point \(Ax_0\) for some \(x_0 \in F^n\). How do we find \(x_0\)?.

Note: \[A \in M_{m \times n}(F), A^* = \overline{A^T} \in M_{n \times m}(F), \ip{Ax}{y}_m = \ip{x}{A^*y}_n\]
Remember that \(rank(A) = n - \dim N(A)\)
\begin{lemma}
\[rank(A*A) = rank(A)\]
i.e. \(N(A*A) = N(A)\)
\end{lemma}
\begin{proof}
If \(x \in N(A)\), \(A^*Ax = A^*0 = 0 \implies N(A) \subset N(A^*A)\).
If \(x \in N(A*A)\), 
\[0 = \ip{A*Ax}{x} = \ip{Ax}{A**x} = \ip{Ax}{Ax} \iff Ax = 0\]
\[ \iff x \in N(A) \implies N(A^*A) \subset N(A)\]
So \(N(A) = N(A^*A)\)
\end{proof}
Trick: 
\[y - Ax_0 \in W^\perp \implies 0 = \ip{Ax}{y - Ax_0} = \ip{x}{A^*(y - Ax_0)} \ \forall \ x \in F^n\]
\[\implies A^*(y - Ax_0) = 0 \iff A^*y = A^*Ax_0\]
Suppose \(rank(A) = n\) i.e. \(N(A) = 0\). Then \(rank(A^*A) = n\) i.e. \(A^*A\) is invertible. Thus
\[x_0 = (A^*A)^{-1}A^*y\]
\vspace{5mm}
\underline{Example: Least Squares Fit:}
Imagine you have some data \(y_1,...,y_n\) given at \(t_1,...,t_n\). What you'd like to do is to model this with a line. Consider using the line \(y = cx + b\) as our model, giving predictions \(ct_1 + b,...,ct_n + b\) for each point.
So consider the data vector
\[y = \begin{pmatrix} y_1 \\ \vdots \\ y_n \end{pmatrix}\]
and express the model as
\[Ax = \begin{pmatrix} t_1 & 1 \\ \vdots & \vdots \\ t_n & 1\end{pmatrix}\begin{pmatrix} c \\ d \end{pmatrix}\]
We then try to find the closest fit to the equation
\[Ax = y\]
If we have a weird model, like trying to fit sine and cosine, we could just set
\[A = \begin{pmatrix}\sin t_1 & \cos t_1 \\ \vdots & \vdots \\ \sin t_n & \cos t_n\end{pmatrix}\]
We could do this for any polynomial (increasing the dimension of \(A\) appropriately) and more!
\end{document}