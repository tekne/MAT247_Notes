\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT247 Notes: Diagonalizability}
\author{Jad Elkhaleq Ghalayini}
\date{January 16 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}

\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{corollary}{Corollary}
\newtheorem*{fact}{Fact}
\newtheorem*{note}{Note}

\usepackage{tikz}

\begin{document}

\maketitle

Let \(T: V \to V\) be a linear transformation, \(\dim V = n < \infty\). If \(\lambda\) is an \underline{eigenvalue}, \(E_\lambda = N(T - \lambda I)\).

When is \(T\) diagonalizable?
\begin{fact}
\begin{enumerate}

    \item If \(T\) is diagonalizable, then for \(\lambda_1,...,\lambda_n \in F\),
    \[P_T(t) = (-1)^n\prod_{k = 0}^n(t - \lambda_k)\]
    splits completely
    
    \item If \(T\) is diagonalizable, and \(\lambda\) is a root of \(P_T(t)\),
    \[P_T(t) = (-1)^n\prod_{k = 1}^k(t - \lambda_k)^{m_{\lambda_k}}\]
    where \(m_\lambda\) is the multiplicity of \(\lambda\) as a root. Then \(\dim E_\lambda = m_\lambda\).
    
    \item \begin{theorem}
    If \(\lambda\) is an eigenvalue of \(T\) with multiplicity \(m_\lambda\), then \[1 \leq \dim E_\lambda \leq m_\lambda\]
    \end{theorem}
    \begin{proof}
    \(E_\lambda \subseteq V\), \(\dim E_\lambda  = p\).
    Let \(v_1,...,v_p\) be a basis for \(E_\lambda\). Extend to a basis
    \[\beta = v_1,...,v_p,v_{p + 1},...,v_n\]
    for \(V\).
    \[[T]_\beta = \begin{pmatrix}\lambda & 0 & ... & 0 & * & ... & * \\ 
    0 & \lambda & ... & 0 & * & ... & * \\ 
    \vdots & \vdots &\ddots & \vdots & \ddots & \vdots \\
    0 & 0 & ... & \lambda & * & ... & * \\
    0 & 0 & ... & 0 & * & ... & * \\
    \vdots & \vdots & \ddots & \vdots & \vdots & \ddots & \vdots \\
    0 & 0 & ... & 0 & * & ... & *\end{pmatrix} = \begin{pmatrix} I\lambda & * \\ 0 & *_t \end{pmatrix}\]
    Note that \[\begin{vmatrix} A & B \\ 0 & C\end{vmatrix} = \det(A)\det(C)\] implying that
    \[P_T(t) = \det([T]_\beta) =  (-1)^p(t - \lambda)^p\det(*_t) \implies m_\lambda \geq p\]
\end{proof}
\end{enumerate}
\end{fact}
\begin{theorem}
Let \(T: V \to V\) be linear, \(\dim V = n < \infty\).
Suppose \[P_T(t) = (-1)^n\prod_{i  = 1}^k(t - \lambda_i)^{m_{\lambda_i}}\]
Then \begin{enumerate}
    \item \(T\) is diagonalizable \(\iff \dim E_{\lambda_i} = m_{\lambda_i} \ \forall \ i\)
    \item If \(\beta_i\) is a basis for \(E_{\lambda_i}\), then
    \[\beta = \beta_1 \cup ... \cup \beta_k \ \text{is a basis for }V\text{, for which}\]
   
    \[[T]_\beta = \begin{pmatrix} \lambda_1 \cdot I_{m_{\lambda_1}} & ... & 0 \\ \vdots & \ddots & \vdots \\ 0 & ... & \lambda_k \cdot I_{m_{\lambda_k}}\end{pmatrix}\]
\end{enumerate}
\end{theorem}
\begin{proof}
\begin{enumerate}

    \item We see that ``\(\implies\)'' is already proven, so we prove ``\(\Leftarrow\)'' with (2)
    
    \item Suppose \(\dim E_{\lambda_i} = m_{\lambda_i}\).
    Steps:
    \begin{enumerate}[label=(\roman*)]
        \item \underline{Lemma:} If \(T: V \to V\) is linear and \(\lambda_1, ..., \lambda_k\) are eigenvalues and \(v_1, ..., v_k\) are eigenvalues,
        then \(\{v_1,...,v_k\}\) is linearly independent (proved earlier)
        \item \underline{Lemma:} If \(T: V \to V\) is linear and \(\lambda_1, ..., \lambda_k\) are eigenvalues, \(w_1,...,w_k\) are vectors in \(E_{\lambda_i},...,E_{\lambda_k}\) respectively then \(w_1 + w_k = 0 \iff w_i = 0 \ \forall i\)
        \item \underline{Proposition:} If \(T: V \to V\) is linear and \(\lambda_1, ..., \lambda_k\) are eigenvalues, and
        \(S_i \subseteq E_{\lambda_i}\) is a finite set of vectors such that \(S_i\) is linearly independent, then
        \[S = S_i \cup ... \cup S_k \text{ is linearly independent}\]
        \begin{proof}
        Write out, for each element \(s_1,...,s_j \in S\) and for some \(a_1,...,a_j \in F\),
        \[\sum_{i = 1}^ja_is_i = 0\]
        For any vectors \(z_1,...,z_r \in E_\lambda\) and scalars \(c_1,...,c_r \in F\),
        \[\sum_{i = 1}^rc_iz_i = w \in E_\lambda\]
        since the eigenspace is a subspace. Hence, we can rewrite the above sum as \(k\) \(w_i \in E_{\lambda_i}\), each formed by a linear combinations of elements in \(S_i\), giving
        \[\sum_{i = 1}^ja_is_i = \sum_{i = 1}^kw_i = 0 \iff w_1 = ... = w_k = 0\]
        by Lemma 2, implying, since each \(S_i\) is linearly independent, that \(a_1 = ... = a_j = 0\)
        \end{proof}
    \end{enumerate}
    
    Choose a basis for each \(E_{\lambda_i}\), labelled \(S_i\). Because they are a basis for \(E_{\lambda_i}\), they are linearly independent, and hence their union
    \[\beta = S_1 \cup ... \cup S_k\]
    is linearly independent by the Proposition, and hence, forms a basis for \(V\), since it has \(m_{\lambda_1} + ... + m_{\lambda_k} = n\) elements.
\end{enumerate}
\end{proof}
\subsection*{Application of Diagonalization: Fibonnaci \#'s}
We have the Fibonnaci numbers
\[1 \ 1 \ 2 \ 3 \ 5 \ 8 \ 13...\]
defined by the property \(F_{n + 1} = F_n + F_{n - 1}, F_1 = 1, F_0 = 1\). So we can write
\[\begin{pmatrix}F_{n + 2} \\ F_{n + 1}\end{pmatrix} = \begin{pmatrix} 1 & 1 \\ 1 & 0\end{pmatrix}\begin{pmatrix}F_{n + 1}\\F_{n}\end{pmatrix}\]
Diagonalization can give you a closed formula for these.
\end{document}