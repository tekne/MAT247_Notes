\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT247 Notes: Review Session}
\author{Jad Elkhaleq Ghalayini}
\date{April 17 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{cancel}

\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{corollary}{Corollary}
\newtheorem*{fact}{Fact}
\newtheorem*{note}{Note}
\newtheorem*{claim}{Claim}

\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}


\newcommand{\defeq}[0]{\vcentcolon=}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\nats}[0]{\mathbb{N}}

\usepackage{tikz}

\begin{document}

\maketitle

You won't be given some matrix and asked ``find the rational canonical form.'' Jordan canonical form is fair game, but not rational.

\section{Minimal Polynomials}

Consider a matrix \(A\) with
\[f(t) = P_A(t) = (\pm 1)(t^n + ...)\]
The Cayley-Hamilton Theorem tells us that
\[f(A) = (\pm 1)(A^n + ...) = 0\]
i.e. that the matrix ``satisfies'' it's own characteristic polynomial.

Consider
\[f(t) = (t - 3)^2(t - 1)(t + 1), g(t) = (t -3)(t - 1)(t + 1)\]
with
\[A = \begin{pmatrix} 3 & & & \\ & 3 & & \\ & & 1 & \\ & & & -1 \end{pmatrix}\]
Clearly,
\[g(A) = \begin{pmatrix} 0 & & & \\ & 0 & & \\ & & -2 & \\ & & & -4 \end{pmatrix}\begin{pmatrix} 2 & & & \\ & 2 & & \\ & & 0 & \\ & & & -2 \end{pmatrix}\begin{pmatrix} 4 & & & \\ & 4 & & \\ & & 2 & \\ & & & 0 \end{pmatrix} = 0\]
You can clearly ``see'' the Cayley-Hamilton theorem in action here, we're cancelling out each eigenvalue one at a time. Note that we \textit{don't} need to square the first matrix. However, if we had
\[A = \begin{pmatrix} 3 & 1 & & \\ & 3 & & \\ & & 1 & \\ & & & -1 \end{pmatrix}\]
we would. Similarly, the minimal polynomial needs to have the same roots as the characteristic polynomial: it's just eliminating unnecessary exponentiation. So if we have \((t - 3)^4\) as our characteristic polynomial, for example, we could have minimal polynomials
\[(t - 3), (t - 3)^2, (t - 3)^3, (t - 3)^4\]
corresponding to Jordan canonical forms with the largest Jordan block being of order 1, 2, 3 and 4 respectively.

So I might, for example, give you
\[P_A(t) = (t + 1)^5(...), M_A(t) = (t + 1)^3(...)\]
and ask you what the \textit{possible} Jordan canonical forms are. We can clearly see how to do this, then: for example, in this case, the minimal polynomial tells us that the largest Jordan block with eigenvalue 1 is of size 3.

\section{Rational Canonical Form}

Consider a vector space over the reals with
\[P_A(t) = t^2 + 1\]
This is clearly irreducible. If it was over a weird field, like \(F_2\), you'd have to know which polynomials are irreducible, but you won't be asked anything of the sort on this exam. We know this has companion matrix
\[A = \begin{pmatrix} 0 & - 1 \\ 1 & 0 \end{pmatrix}\]
which would give us our rational canonical form. Easy. Let's try a slightly trickier case:
\[P_A(t) = (t^2 + 1)^2\]
We can immediately write down one obvious possibility for \(A\):
\[A = \begin{pmatrix} 0 & -1 && \\ 1 & 0 && \\ && 0 & - 1 \\ && 1 & 0\end{pmatrix}\]
There's another possibility, analogous to that in the Jordan block case. If we had \((t - 2)^2\), then we could have canonical forms
\[\begin{pmatrix} 2 & 0 \\ 0 & 2 \end{pmatrix}, \begin{pmatrix} 2 & 1 \\ 0 & 2 \end{pmatrix}\]
So similarly, we could write down the companion matrix to \((t^2 - 1)^2 = t^4 - 2t^2 + 1\) itself and get
\[A = \begin{pmatrix} 0 &&& 1 \\ 1 & 0 && 0 \\ & 1 & 0 & -2 \\ && 1 & 0 \end{pmatrix}\] 
Hence, in the first case, we would have \(M_A(t) = t^2 + 1\), whereas in the second case we would have \(M_A(t) = P_A(t) = (t^2 + 1)^2\).

So suppose, for example, we had \(P_A(t) = (t^2 + 1)^3(t^2 + 5)^5\). The possibilities for the minimal polynomial would then be
\[M_A(t) = (t^2 + 1)^i(t^2 + 5)^j\]
where \(0 \leq i \leq 3, 0 \leq j \leq 5\).

\section{Spectral Theorem}

Question: Let \(V, \ip{}{}\) be a finite dimensional IPS over \(\comps\), with \(\dim_\comps V = n\) and let \(T: V \to V\) be a normal linear transformation of \(V\)

\begin{enumerate}[label=(\roman*)]

    \item State the spectral theorem:
    \begin{theorem}[Spectral Theorem]
    \end{theorem}
    
    \item Suppose that \(T\) has \(n\) distinct eigenvalues and that \(S\) is a linear transformation of \(V\) such that \(ST = TS\). Show that \(S\) is normal.
    
    Take \(v_j\) to be an eigenvector corresponding to \(\lambda_j\).
    \[TS(v_j) = ST(v_j) = \lambda S(v_j) \implies S(v_j) \in E_{\lambda_j}\]
    Since we have \(n\) distinct eigenvalues, we know that each distinct eigenspace must be of dimension 1, so
    \[S(v_j) \in E_{\lambda_j} \implies S(v_j) = \mu_jv_j\]
    for some \(\mu_j \in F\). Hence, taking \(S\) with respect to the basis \(\beta = \{v_1,...,v_n\}\), we'll get
    \[[S]_\beta = \begin{pmatrix} \mu_1 && \\ &\ddots& \\ && \mu_n \end{pmatrix}\]
    implying that \(S\) is normal.
    
    \item Show that there is a polynomial \(g(t)\) such that \(S = g(T)\)
    
    We can use Lagrange interpolation to construct a polynomial such that
    \[\forall j \in \nats, j \leq n, g(\lambda_j) = \mu_j\]
    So
    \[g(T)(v_j) = g(\lambda_j) \cdot v_j = \mu_jv_j \implies g(T) = S\]
    
    \item Give an example of such a pair of operators \(S\) and \(T\) where \(T\) does \textit{not} have \(n\) distinct eigenvalues and \(S\) \textit{cannot} be written as a polynomial in \(T\)
    
    We need to find two transformations \(T, S = normal\) such that
    \[ST = TS\]
    such that \(S\) is not a polynomial in \(T\). Consider \(T = I\). Clearly, anything that isn't scalar multiplication can't be written as a polynomial in \(I\), but \(I\) is normal and commutes with anything. The key detail necessary for the proof above is ``suppose that \(T\) has \(n\) distinct eigenvalues.''

\end{enumerate}

\end{document}