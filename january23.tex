\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT247 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{January 23 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}

\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{corollary}{Corollary}
\newtheorem*{fact}{Fact}
\newtheorem*{note}{Note}

\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}

\usepackage{tikz}

\begin{document}

\maketitle

Recall:
\begin{itemize}
    \item \(V\) is a vector space over \(F\), \(\dim V = n\)
    \item \(T: V \to V\)
    \item For any \(v \neq 0, v \in V\), we have
    \[W = <v>_T = span\{v, T(v), T^2(v)...\}\]
\end{itemize}
Facts:
\begin{itemize}
    \item \(W\) is \(T\)-invariant, \(T_W: W \to W\)
    \item So \(P_{T_W}(t)|P_T(t)\)
    \item Let \(\beta_w = \{v, T(v),..., T^{k-1}(v)\}\) be a basis for \(W\).
    \[T^{k}(v) + a_{k-1}T^{k-1}(v) + ... + a_0v = 0\]
    and:
    \[[T_W]_{\beta_W} = \begin{pmatrix}
    0 & 0 & ... & - a_0 \\
    1 & 0 & ... & -a_1 \\
    \vdots & \vdots & \ddots & \vdots \\
    0 & ...& 0 & -a_{k-1} \\
    0 & ... & 1 &- a_{k - 1}
    \end{pmatrix}\]
    giving
    \[P_{T_W}(t) = (-1)^k(t^k + a_{k - 1}t^{k-1} + ... + a_0)\]
    This can, potentially, allow us to find the determinant more easily.
\end{itemize}
Idea: Suppose \(V = W = <v>_T\). That means
\begin{itemize}
    \item \(P_T(t) = (-1)^k(t^k + a_{k - 1}t^{k-1} + ... + a_0)\)
    where
    \item \(T^n(v) + a_{n - 1}T^{n - 1}(v) + ... + a_1v = 0\)
    But this means that
    \item \((T^n + a_{n - 1}T^{n - 1} + ... + a_1I)v = (-1)^nP_T(T)(v) = 0\)
\end{itemize}
\begin{theorem}
\[f(t) = P_T(t) = (a_0 + a_1t + ... + a_{n - 1}t^{n - 1} + t^n)(-1)^n \implies f(T) = \underline{0}\]
\end{theorem}
\begin{proof}
If \(v = 0\), we have \(f(T)(v) = 0\). If \(v \neq 0\), let \(W = <v> \subseteq V\)
Then let \(g(t) = P_{T_w}(t)\). We have \(f(t) = h(t)g(t)\), i.e. \(g(t)|f(t)\). Then,
\[f(T)(v) = (h(T)g(T))(v) = h(T)(g(T)(v)) = h(T)(0) = 0\]
\end{proof}
Now, we can aunch into chapter 6:
\section{Chapter 6: Inner Product Spaces}
Consider the picture of \(\mathbb{R}^2\)

\begin{tikzpicture}
\draw(0, 0) -- (1, 2);
\draw(2, 0) -- (0, 2);
\end{tikzpicture}

That's wrong, but what's wrong with it? The angles! We know \(\mathbb{R}^2\) must look like this

\begin{tikzpicture}
\draw(0, -1) -- (0, 1);
\draw(-1, 0) -- (1, 0);
\end{tikzpicture}

In our current understanding of vector spaces, we can't tell the difference. So we introduce the Inner Product, the analog of the dot product. Note that for the rest of this discussion, we assume \(F = \mathbb{R}\) or \(\mathbb{C}\).

\begin{definition}
\(V\) is an Inner Product Space (IPS), if we have:
\[\ip{x}{y} \in F, x, y \in V\]
for any \(x \in V, y \in V\) satisfying the following axioms
\begin{enumerate}[label = (\alph*)]
    \item \(\ip{x + z}{y} = \ip{x}{y} + \ip{z}{y}\)
    \item \(\ip{cx}{y} = c\ip{x}{y}, c \in F\)
    \item \(\ip{y}{x} = \Bar{\ip{x}{y}}\)
    \item \(\ip{x}{x} > 0\) if \(x \neq 0\)
\end{enumerate}
\end{definition}
We can have multiple inner product spaces on the same vector space.


Recall that if \(z = a + ib \in \mathbb{C}\), \(\bar{z} = a - ib \in \mathbb{C}\). This is known as the complex conjugate. In the real case, nothing happens, so the bar is irrelevant, giving \(\ip{y}{x} = \ip{x}{y}\). Note that \(z = \bar{z} \iff b = 0 \iff z \in \mathbb{R}\), and \(z\bar{z} = a^2 + b^2 = |z|^2\).
\subsection*{Standard example}
\(V = F^n\), \[\ip{x}{y} = x_1\bar{y_1} + x_2\bar{y_2} + ... + x_n\bar{y_n}\]
Exercise: prove this is an inner product
On \(\mathbb{R}^3\), \(\ip{x}{y} = x_1y_1 + x_2y_2 + x_3y_3\), the dot product \(x \dot y\). Hence, we get
\(||x|| = \sqrt{x_1^2 + y_1^2 + z_1^2}\).
\begin{definition}
\[||x||^2 = \ip{x}{x}\]
\end{definition}
Example: \(V = C([0, 1])\),
\[\ip{f}{g} = \int_0^1f(t)g(t)dt\]
\[\ip{f}{f} = \int_0^1f(t)^2dt > 0 \iff f(t) \neq 0\]
Notice, for this space, we have the notion of lengths and angles. We can talk about the angle between \(\sin\) and \(\cos\) too! The point is, inner product spaces allow us to give a geometry to vector places.
\end{document}