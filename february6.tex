\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT247 Notes: Orthonormal projections}
\author{Jad Elkhaleq Ghalayini}
\date{February 6 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}

\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{corollary}{Corollary}
\newtheorem*{fact}{Fact}
\newtheorem*{note}{Note}

\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}


\newcommand{\defeq}[0]{\vcentcolon=}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\comps}[0]{\mathbb{C}}

\usepackage{tikz}

\begin{document}

Recall: if \(W\) is an IPS over \(F = \reals, \comps\) where \(\dim W = m < \infty\), we can find an orthonromal basis for \(W\), \(w_1,...,w_m\). For such a basis,
\[w = \sum_{k = 1}^m\ip{w}{w_k}w_k\]

Now suppose \(V\) is an IPS (which \underline{can} be infinite dimensional), \(W \subset V\) is a finite dimensional subspace and \(\dim W = m\).
\begin{theorem}
\begin{enumerate}
    \item Any \(y \in V\) has a unique expansion \(y = u + z\) with \(u \in W, z \in W^\perp\)
    \item If \(w_1,...,w_m\) is an orthonormal basis for \(W\), then
    \[u = \sum_{k = 1}^n\ip{y}{w_k}w_k\]
    or for an orthogonal basis
    \[u = \sum_{k = 1}^n\frac{\ip{y}{w_k}}{\ip{w_k}{w_k}}w_k\]
    and \(z = y - u\)
    \item \(u\) is the unique vector in \(W\) closest to \(y\). In other words,
    \[||y - u|| \leq ||y - u'|| \ \forall \ u' \in W, ||y - u'|| = ||y - u|| \iff u' = u\]
\end{enumerate}
\end{theorem}
\begin{proof}
Start with (2): Take \(w_1,...,w_m\) to be an orthonormal basis for \(W\) and let \(u\) be as defined above. Certainly \(y = u + z\) and \(u \in W\) since \(u\) is a linear combination of vectors in \(W\). We must show that \(z \in W^\perp\).
\[z \in W^\perp \iff \ip{z}{w_j} = 0 \ \forall \ w_j \in \{w_1,...,w_m\}\]
We check
\[\ip{z}{w_j} = \ip{y - u}{w_j} = \ip{y}{w_j} - \ip{u}{w_j} = \ip{y}{w_j} - \ip{\sum_{i = 1}^m\ip{y}{w_i}w_i}{w_j}\]
\[= \ip{y}{w_j} - \sum_{i = 1}^m\ip{y}{w_i}\ip{w_i}{w_j}\]
Since \(\{w_1,...,w_m\}\) is orthonormal, we get
\[= \ip{y}{w_j} - \ip{y}{w_j}(1) = 0\]

So every \(y\) has a decomposition \(y = u + z\). Now we need to prove uniquness to finish proving (1). Let \(y = u' + z'\) be another decomposition such that \(u' \in W, z' \in W^\perp\).
\[u + z = u' + z' \implies u - u' = z' - z, u - u' \in W, z - z' \in W^\perp\]
\[\implies u - u',z - z' \in W \cap W^\perp\]
Since \(W \cap W^\perp = \{0\}\), hence 
\[u - u' = z' - z = 0 \implies u = u', z = z'\]

Now we must prove (3). To do this, suppose we have any vector \(u' \in W\).
\[||y - u'||^2 = ||y + u - u - u'||^2 = \ip{y - u + u - u'}{y - u + u - u'}\]
Remember that \(y - u \in W^\perp, u - u' \in W\). So we only have the terms
\[||y - u'||^2 = \ip{y - u}{y - u} +\ip{u - u'}{u - u'} = ||y - u||^2 + ||u - u'||^2\]
\(||u - u'||^2 > 0 \ \forall \ u' \in W, u' \neq u\). Hence, \(u\) is the closest vector to \(y\).
\end{proof}
\begin{theorem}
Let \(V\) be a finite dimensional IPS, \(W \subset V\) be a subspace.
\begin{enumerate}
    \item Any orthonormal basis for \(W\) \(w_1,...,w_m\) can be extended to an orthonormal basis
    \[\{w_1,...,w_m,w_{m + 1},...,w_n\}\]
    \item \(W^\perp = span\{w_{m + 1},...,w_n\}\)
    \item Hence \(\dim V = \dim W + \dim W^\perp\)
    \item \((W^\perp)^\perp = W\)
\end{enumerate}
\end{theorem}
\begin{proof}
Take \(w_1,...,w_m\) as a basis for \(W\). We can extend this basis to a (not necessarily orthonormal) basis for \(V\),
\[\beta = \{w_1,...,w_m,v_{m + 1},...,v_n\}\] We apply the Gram-Schmidt orthonormalization process to \(\beta\). Since \(w_1,...,w_m\) are orthonormal, they remain unchanged,but \(v_{m + 1},...,v_n\) become the orthonormal \(w_{m + 1},...,w_n\) giving an orthonormal basis for \(V\)
\[\beta' = \{w_1,...,w_m,w_{m + 1},...,w_n\} \supset \{w_1,...,w_m\}\]
proving (1). For (2), let \(S_1 = \{w_{m + 1},...,w_n\}\), and note \(S_1 \subset W^\perp\). \(S_1\) is linearly independent. We have that,
\[\forall \ x \in W^\perp, x = \sum_{k = 1}^n\ip{x}{w_k}w_k\]
But
\[x \in W^\perp \implies \ip{x}{w_k} = 0 \forall k \in \mathbb{N}, k \leq m\]
So
\[x = \sum_{k = m + 1}^n\ip{x}{w_k}w_k\]
which is a linear combination of elements in \(S_1\). Hence, \(S_1\) spans \(W^\perp\) and is therefore a basis for \(W^\perp\) proving (2), (3).

Finally, we prove (4). Claim: \((W^\perp)^\perp = W\). We know \(W^\perp\) is a finite dimensional subset of \(V\). Hence, we can apply part (3) of the Theorem to see that
\[\dim W^\perp + \dim (W^\perp)^\perp = \dim V, \dim W + \dim W^\perp = \dim V \implies \dim W = \dim (W^\perp)^\perp\]
So \(W = (W^\perp)^\perp\) are equal, since they have the same dimension.
\end{proof}

\section{Introduction to Adjoints}
So far we've talked about inner product spaces. Now, we're interested in the interaction of inner product spaces and linear transformations.

Our goal is to show that if \(T: V \to V\) respects the inner product in a certain way, than a lot of properties, including about diagonalization, fall out.
\begin{definition}
An \underline{adjoint} of \(T\) is a linear transformation \(S: V \to V\) such that
\[\ip{T(x)}{y} = \ip{x}{S(y)} \ \forall \ x, y \in V\]
\end{definition}
\(S\) is \underline{unique} \underline{\underline{if}} it exists.
\begin{proof}
Suppose \(S, S'\) are adjoints.
\[\ip{T(x)}{y} = \ip{x}{S(y)} = \ip{x}{S'(y)}\]
Thus
\[\ip{T(x)}{S(y) - S'(y)} = 0 \ \forall \ x \in V \implies S(y) - S'(y) = 0 \ \forall \ y \in V \implies S = S'\]
\end{proof}
Next time, we'll prove an adjoint always exists in the finite dimensional case.
\end{document}