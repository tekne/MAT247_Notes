\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT247 Notes: Orthonormal Basis}
\author{Jad Elkhaleq Ghalayini}
\date{January 18 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{cancel}

\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{corollary}{Corollary}
\newtheorem*{fact}{Fact}
\newtheorem*{note}{Note}
\newtheorem*{goal}{Goal}

\newcommand{\real}[0]{\mathbb{R}}
\newcommand{\comp}[0]{\mathbb{C}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}

\usepackage{tikz}

\begin{document}

\maketitle

\begin{goal}
Let \(V, \langle, \rangle\) be an IPS, \(\dim V = n < \infty\).
\begin{theorem}
\(V\) has an \underline{orthonormal basis}
\[\beta = \{v_1,...,v_n\}, \ \ \ip{v_i}{v_j} = \delta_{ij}, ||v_i|| = 1\]
\end{theorem}
\end{goal}
Consider \(V = \real^2\), and let
\[v_1 = (1, 2), v_2 = (-1, 1), \ip<v_1,v_2>= 1, \theta = ...\] 
We have basis \(S = \{v_1, v_2\}\) and we want to find an orthogonal basis \(S' = \{w_1,w_2\}\).
Let's modify \(v_2\) to set a vector \(\perp\) to \(v_1\). To preserve linear independence, let's try to subtract a multiple of \(v_1\)
\[w_1 = v_1, \ \ w_2 = v_2 - cv_1, c \in F\]
\[0 = \ip{w_2}{v_1} = \ip{v_2 - cv_1}{v_1} = \ip{v_2}{v_1} - c\ip{v_1}{v_1}\]
So we get
\[c = \frac{\ip{v_2}{v_1}}{\ip{v_1}{v_1}}\]
Thus, 
\[w_2 = v_2 - \frac{\ip{v_2}{v_1}}{\ip{v_1}{v_1}}v_1\]
Keep track of the constants in this calculation. If you don't do it in a fairly standard, systematic way, you'll always make mistakes (from experience). We already know \(\ip{v_2}{v_1}\) from before, so let's calculate
\[\ip{v_1}{v_1} = 1 \cdot 1 + 2 \cdot 2 = 5\]
So we get
\[w_2 = \begin{pmatrix}-1 \\ 1\end{pmatrix} - \frac{1}{5}\begin{pmatrix}1 \\ 2\end{pmatrix} = \begin{pmatrix}-6/5 \\  3/5\end{pmatrix}\]
\underline{More generally}, let \(V, \langle, \rangle\) be \underline{any} IPS and let
\[S = \{v_1,...,v_k\} \text{ be linearly independent}\]
we can construct \(S' = \{w_1,...,w_k\}\) such that
\begin{enumerate}
    \item \(w_1 = v_1\)
    \item \(S'\) is \underline{orthogonal}
    \item \(span(S') = span(S)\)
\end{enumerate}
This is called the
\subsection*{Gram-Schmidt Orthogonalization Process (GSOP)}
\[w_1 = v_1\]
\[w_2 = v_2 - \frac{\ip{v_2}{w_1}}{\ip{w_1}{v_1}}w_1\]
Now let's figure out how to get \(w_3\). We've got \(w_1, w_2\) perpendicular to each other, and spanning the same thing as \(v_1, v_2\). Now we have a third vector, \(v_3\). It cannot lie in that plane, because \(v_3 \notin span\{v_1,v_2\} = span\{w_1,w_2\}\). So we can again make it orthogonal by subtracting off components of the vector parallel to the plane \(w_1, w_2\). So let's try to use an analogous formula, and make
\[w_3 = v_3 - c_1w_1 - c_2w_2\]
We want
\[\ip{w_3}{w_1} = \ip{w_3}{w_2} = 0\]
So we get
\[\ip{w_3}{w_1} = \ip{v_3}{w_1} - c_1\ip{w_1}{w_1} - \cancel{c2\ip{w_2}{w_1}}\]
\[\ip{w_3}{w_2} = \ip{v_3}{v_2} - \cancel{c_1\ip{w_2}{w_1}} - c_2\ip{w_2}{w_2}\]
We solve, finding that
\[c_1 = \frac{\ip{v_3}{w_1}}{\ip{w_1}{w_1}}, c_2 = \frac{\ip{v_3}{w_2}}{\ip{w_2}{w_2}}\]
So we get a formula
\[w_3 = v_3 - \frac{\ip{v_3}{w_1}}{\ip{w_1}{w_1}}w_1 - \frac{\ip{v_3}{w_2}}{\ip{w_2}{w_2}}w_2\]
Similarly, we get
\[w_4 = v_3 - \frac{\ip{v_4}{w_1}}{\ip{w_1}{w_1}}w_1 - \frac{\ip{v_4}{w_2}}{\ip{w_2}{w_2}}w_2 = \frac{\ip{v_4}{w_3}}{\ip{w_3}{w_3}}w_3\]
You keep going to get
\[w_n = v_n - \sum_{i = 0}^{n - 1}\frac{\ip{v_n}{w_i}}{\ip{w_i}{w_1}}w_i\]
We have an additional nice property: for any \(j < k\), if we just truncate the process for \(v_1,...,v_j\), we still have
\[span\{v_1,...,v_j\} = span\{w_1,...,w_j\}\]
If we really want an orthonormal basis, we can just quickly normalize each vector afterwards. On the other hand, we don't do this DURING the algorithm when calculating by hand, since you end up with a bunch of square roots everywhere. Now, we can prove Theorem 1 above.
\begin{proof}{Theorem 1}
Let \(\{v_1,...,v_n\}\) be any basis for \(V\). Apply GSOP to get an orthogonal basis \(\{w_1,...,w_n\}\). Then, normalize each vector to get the orthonormal basis \(\{\frac{w_1}{||w_1||},...,\frac{w_n}{||w_n||}\}\).
\end{proof}
Example: \[V = P(\real) = \text{ polynomials }, \ \ \ip{f}{g} = \int_{-1}^1f(t)g(t)dt\]
Let \(S = \{1,t,t^2,...\}\)
If we apply the GSOP, we get another set of polynomials
\[S' = \{1,...\}\]
called the Legendre polynomials. If you look at the graph, all the zeroes are concentrated between \([-1, 1]\)
\end{document}
