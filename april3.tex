\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT247 Notes: Groups}
\author{Jad Elkhaleq Ghalayini}
\date{April 3 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{cancel}

\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{corollary}{Corollary}
\newtheorem*{fact}{Fact}
\newtheorem*{note}{Note}
\newtheorem*{claim}{Claim}

\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}


\newcommand{\defeq}[0]{\vcentcolon=}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\nats}[0]{\mathbb{N}}

\usepackage{tikz}

\begin{document}

\maketitle

Let \(V\) be a finite dimensional vector space over \(F\). Recall the group
\[GL(V) = \{T: V \to V | T \text{ invertible}\}\]
Recall, if \(V = F^n\), we have
\[GL(V) \approx GL_n(F) = \{x \in M_n(F) | \det x \neq 0\}\]
which has subgroup
\[B = \{g \in GL_n(F) | g = \text{ upper triangular}\}\]
which has subgroup
\[D = \{g \in B | g = \text{ diagonal}\}\]
We can write down a lot of statements that we learned about matrices in terms of groups. As an example, let's do the
\section{Gram-Schmidt Orthonormalization Process}
Consider, to keep it concrete, \(V = \reals^n\) with the standard inner product, and let \(v_1,...,v_n\) be a basis. We can get an orthogonal basis
\[w_1 = v_1, \ w_2 = v_2 - \frac{\ip{v_2}{w_1}}{\ip{w_1}{w_1}}w_1, ...\]
which we can then proceed to normalize to
\[u_1 = \frac{w_1}{||w_1||}, u_2 = \frac{w_2}{||w_2||},...\]
So let's write this in group theoretic language: let
\[(v_1 ... v_n) = g \in M_n(\reals), g \in GL_n(\reals) \iff \text{ cols are a basis}\]
We want to get
\[(u_1 ... u_n) = u \in GL_n(\reals)\]
where \(u_1,...,u_n\) is an orthonormal basis, which means
\[u^*u = I \iff u \in O(n)\]
So what are we doing to get this matrix? In the first step, we take \(g \cdot \mathbf{e}_1\), to isolate the first column of \(g\), \(v_1\). On the other hand, to get the second element, we take \(g \cdot \mathbf{e}_2 + ag \cdot \mathbf{e}_1\), since we're taking \(v_2\), and then subtracting some multiple of \(v_1\). Hence, overall, we have
\[(w_1 ... w_n) = (v_1 ... v_n)\begin{pmatrix} 1 & * & ... & * \\ 0 & 1 & ... & * \\ \vdots & \vdots & \ddots  & \vdots \\ 0 & 0 & 0 & 1 \end{pmatrix} = (w_1 ... w_n)h\]
We then want to orthonormalize, by taking
\[u = (u_1 ... u_n) = (w_1 ... w_n)\begin{pmatrix} ||w_1||^{-1} & & \\ & \ddots & \\ & & ||w_n||^{-1} \end{pmatrix} = (w_1 ... w_n)d\]
So what we're really saying is that if we have an upper triangular matrix \(h\), and a diagonal matrix \(d\), we can write
\[g \cdot h \cdot d = u\]
Writing \(h \cdot d = b \in B\), we have
\[g \cdot b = u \iff g = u \cdot b^{-1}\]
So what the GSOP is really telling us is
\[GL_n(\reals) = O(n) \cdot B\]
\section{Rotations and Reflections}
Consider \(V = \reals^2\) with the standard inner product, and consider \(g \in O(2) \subset GL_2(\reals) \implies g^*g = I \iff ||g(x)|| = ||x||\). We have subgroup
\[SO(2) = \{g \in O(2) | \det g = 1\}\]
But let \(g \in O(n)\). We have
\[g^* = g^T \implies \det(g^*) = \det(g^T) = \det(g)\]
\[\implies \det(g^*g) = \det(g^2) = \det(I) = 1 \implies \det(g) = \det(g^*) = \pm 1\]
Now, this actually implies every element in \(O(2)\) is either a rotation or reflection. This is in the book, but not on the test.
\begin{itemize}
    \item Suppose \(\det(g) = +1, g \in SO(2)\). So
    \[g = \begin{pmatrix} a & b \\ c & d \end{pmatrix},g^* = \begin{pmatrix} a & c \\ b & d \end{pmatrix} = g^{-1} = \cancel{\frac{1}{\det g}}\begin{pmatrix} d & -b \\ -c & a \end{pmatrix}\]
    implying that we have \(a = d, c = -b\), giving
    \[g = \begin{pmatrix} a & b \\ -b & a \end{pmatrix}, \det g = a^2 + b^2 = 1\]
    So we can pick \(\theta\) such that \(a = \cos\theta, b = \sin\theta\), giving
    \[g = k_\theta = \begin{pmatrix} \cos\theta & \sin\theta \\ -\sin\theta & \cos\theta \end{pmatrix}\]
    
    \item Suppose \(\det(g) = -1, g \in O(2)\). We go through the exact same calculation, except instead of canceling \(\det g\), we have to add a minus sign, giving
    \[\begin{pmatrix} a & c \\ b & d \end{pmatrix} = \begin{pmatrix} -d & b \\ c& -a \end{pmatrix}\]
    implying
    \[g = \begin{pmatrix} a & b \\ b & -a \end{pmatrix} \implies P_g(t) = t^2 - (a^2 + b^2)\]
    giving eigenvalues \(\lambda = \pm 1\), giving a basis \(v_+, v_-\) of eigenvectors, implying that we have a reflection of the \(v_-\) component of all inputs
\end{itemize}

For an example of such rotations and reflections, consider \(n = 2\),
\[Sym(\square) = \{h \in O(2) | h(\square) = \square\} \subset O(2)\]
where \(\square\) is the unit square. Check that this is a subgroup. We have
\[|Sym(\square)| = 8, |Sym^+(\square)| = 4\]
where \(Sym^+(\square) = Sym(\square) \cap SO(2)\).
The number of elements in a finite group is called the ``order'' of the group.

Let's try something more fun: \(n = 3\), \(O(3) \subset GL_3(\reals)\). We have that
\[Sym(cube) \subset O(3)\]
where
\[Sym(cube) = \{h \in O(3) | h(cube) = cube\}\]
It's certainly finite...
\end{document}