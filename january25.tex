\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT247 Notes: Inner Product Spaces}
\author{Jad Elkhaleq Ghalayini}
\date{January 18 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{cancel}

\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{corollary}{Corollary}
\newtheorem*{fact}{Fact}
\newtheorem*{note}{Note}

\newcommand{\real}{\mathbb{R}}
\newcommand{\comp}{\mathbb{C}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}

\usepackage{tikz}

\begin{document}

\maketitle

Consider an IPS \(V\),
\[x, y \in V, F = \real, \comp\]
We have axioms
\begin{enumerate}[label=(\alph*)]
    \item \(\ip{x + z}{y} = \ip{x}{y} + \ip{z}{y}\)
    \item \(\ip{cx}{y} = c\ip{x}{y}\)
    \item \(\ip{y}{x} = \overline{\ip{x}{y}}\)
    \item \(\ip{x}{x} > 0\) if \(x \neq 0\)
\end{enumerate}
``Standard example'':
\[V = F^n, \ip{x}{y} = x_1\bar{y_1} + ... + x_n\bar{y_n}\]
\begin{note}
\begin{itemize}
    \item \[\ip{x}{cy} = \overline{\ip{cy}{x}} = \overline{c\ip{y}{x}} = \bar{c}\overline{\ip{y}{x}} = \bar{c}\ip{x}{y}\]
    \item \[\ip{x}{y + z} = \ip{x}{y} + \ip{x}{z}\]
    This is called \underline{conjugate linearity} in the second argument: scalars do not pull out, but their conjugates do
    \item \[\ip{0}{y} = \ip{0 \cdot 0, y} = 0\ip{0, y} = 0\]
    \item If \[\ip{x}{z} = \ip{y}{z} \ \forall \ z \in V, x = y\]
    \begin{proof}
        \[\ip{x}{z} - \ip{y}{z} = \ip{x - y}{z} = 0 \implies \ip{x - y}{x - y} = 0\]\[ \implies x - y = 0 \implies x = y\]
    \end{proof}
\end{itemize}
\end{note}
The point of this abstract theory is to allow us to define inner product spaces on arbitrary vector spaces even without geometric basis, such as the space of polynomials. For example, take
\[V = P(\real), p, q \in V\] and define
\[\ip{p}{q} = \int_0^1p(t)q(t)dt\]
We claim this is an inner product. Let's check axiom (d):
\[\ip{p}{p} = \int_0^1p(t)^2dt = 0 \iff p(t) = 0\]
We have no geometric intuition about this space: its infinite dimensional! Remember, last time we defined
\[||x|| = \sqrt{\ip{x}{x}}\]
to be the \textit{norm} or \textit{length} of \(x\).
Note \(||x|| = 0 \iff x = 0\)
We could also have
\[\ip{p}{q}' = \int_{-1}^0p(t)q(t)(1 - t^2)dt\]
and we get another inner product. It turns out in the theory of polynomials, there are many different inner products with useful properties.
\begin{theorem}
Let \(V\) be an \(IPS\).
\begin{enumerate}[label=(\alph*)]
    
    \item \(||cx|| = |c|||x||\)
    
    \item \(||x|| = 0 \iff x = 0\)
     
    \item Cauchy-Schwarz inequality:
    \[|\ip{x}{y}| \leq ||x||||y||\]
    
    \item Triangle inequality:
    \[||x + y|| \leq ||x|| + ||y||\]
\end{enumerate}
\end{theorem}
This last one, in geometry, is quite obvious, in the sense that if we add two vectors, the length of the sum is at most the sum of the lengths, but can be less.
\begin{proof}{Cauchy-Schwarz inequality: }
Assume \(y \neq 0\)
\[0 \leq ||x - cy||^2 = \ip{x - cy}{x - cy} = \ip{x}{x} + \ip{x}{-cy} + \ip{-cy}{x} + \ip{-cy}{-cy}\]
\[= \ip{x}{x} - \bar{c}\ip{x}{y} = c\ip{y}{x} + |c|^2\ip{y}{y}\]
Now consider the trick choice of \(c\):
\[c = \frac{\ip{x}{y}}{\ip{y}{y}}\]
We have
\[0 \leq \ip{x}{x} - \frac{\ip{y}{x}}{\ip{y}{y}}\cdot\ip{x}{y} - \cancel{\frac{\ip{x}{y}}{\ip{y}{y}}\cdot\ip{y}{x} + \frac{\ip{x}{y}\ip{y}{x}}{\ip{y}{y}}\ip{y}{y}}\]
\[= \ip{x}{x} - \frac{\ip{x}{y}\ip{y}{x}}{\ip{y}{y}} \implies 0 \leq \ip{x}{x}\ip{y}{y} - \ip{x}{y}\ip{y}{x}\]
\[\implies |\ip{x}{y}|^2 \leq \ip{x}{x}\ip{y}{y} \implies |\ip{x}{y}| = ||x||||y||\]
In the case \(y = 0\), we clearly have
\[|<x, y>| = 0 \leq ||x||||y|| = 0\]
\end{proof}
\begin{proof}{Triangle inequality: }
\[||x + y||^2 = \ip{x + y}{x + y} = \ip{x}{x} + \ip{x}{y} + \ip{y}{x} + \ip{y}{y}\]
\[= ||x|| + 2|Re\ip{x}{y}| + ||y|| \leq ||x|| + 2|\ip{x}{y}| + ||y||\]
Which, by Cauchy-Schwarz, is less than or equal to
\[||x|| + 2||x||||y|| + ||y|| = (||x|| + ||y||)^2\]
\end{proof}
\section{Law of cosines}
In the case of a right triangle, we have
\[C^2 = A^2 + B^2\]
On the other hand, assume we instead have an angle \(\theta\) opposite to \(C\). We have
\[C^2 = A^2 + B^2 - 2AB\cos\theta\]
What's important about the law of cosines is it can allow us to define angles in reverse. Consider two vectors \(x, y \in V\). We want to define the angle \(\theta\) between them. We can generalize the Law of Cosines:
\[||y - x||^2 = ||x||^2 + ||y||^2 + 2||x||||y||\cos\theta\]
\[\iff \ip{y - x}{y - x} = \ip{x}{x} + \ip{y}{y} + 2||x||||y||\cos\theta\]
\[\implies 2||x||||y||\cos\theta = \ip{x}{x} + \ip{y}{y} - \ip{y - x}{y - x}\]
In the \underline{real} case, this implies
\[\implies 2||x||||y||\cos\theta = \ip{x}{x} + \ip{y}{y} - \ip{y}{y} - \ip{x}{x} + 2\ip{x}{y}\]
So we can \underline{define}, assuming \(x, y \neq 0\)
\begin{definition}
\[\cos\theta = \frac{\ip{x}{y}}{||x||||y||} \implies \theta = \arccos\left(\frac{\ip{x}{y}}{||x||||y||}\right)\]
\end{definition}
The problem is this makes sense only if 
\[-1 \leq \frac{\ip{x}{y}}{||x||||y||} \leq 1\]
But Cauchy-Schwarz tells us that exactly this is a condition for real inner product spaces! We add on the following
\begin{definition}
Let \(V\) be an IPS, \(F = \real, \comp\). \(x, y\) are \underline{perpendicular} (written \(x \perp y\)) \(\iff \ip{x}{y} = 0\). The 0 vector is perpendicular to all vectors
\end{definition}
\begin{definition}
Let \(S \subset V\) be a set of nonzero vectors
\begin{itemize}
    \item \(S\) is \underline{orthogonal} \(\iff x, y \in S, x \neq y \implies x \perp y\)
    \item \(S\) is \underline{orthonormal} \(\iff\) \(S\) is orthogonal and \(\forall \ x \in S, ||x|| = 1\)
\end{itemize}
\end{definition}
Let \(S = \{s_1,...,s_k\}\) be an orthonormal set, \[v \in span(S) \implies v = \sum_{i = 1}^ka_is_i\]
Consider
\[\ip{v}{s_j} = \ip{\sum_{i = 1}^ka_is_i}{s_j} = \sum_{i = 0}^ka_i\ip{s_i}{s_j} = a_j\ip{s_j}{s_j} = a_j(1)^2 = a_j\]
Warning for \(\comp\):
\[\ip{s_j}{v} = \bar{c_j}\]
Nothing changes for the case where \(S\) is infinite.
\vspace{5mm}
Example: \(V = P(\real), f, g \in P(\real)\),
\[\ip{f}{g} = \int_0^1f(t)g(t)dt\]
We can now calculate the angle between polynomials!

Consider a complex valued function in \(H = C([0, 2\pi])\). By complex valued, we mean
\[f(t) = a(t) + ib(t) \implies \int_0^{2\pi}f(t)dt = \int_0^{2\pi}a(t)dt + i\int_0^{2\pi}b(t)dt\]
The inner product on \(H\) is given by
\[\ip{f}{g} = \frac{1}{2\pi}\int_0^{2\pi}f(t)g(t)dt\]
\begin{definition}
\(f_n(t) = e^{int}, n \in \mathbb{Z}\)
\end{definition}
We can take complex exponentials using Euler's formula
\[e^{a + ib} = e^a(\cos b + i\sin b)\]
We have the familiar properties \(e^{z_1 + z_2} = e^{z_1}e^{z_2}\), so we get
\[\ip{f_m}{f_n} = \frac{1}{2\pi}\int_0^{2\pi}f_m(t)\bar{f_n(t)}dt = \frac{1}{2\pi}e^{imt}e^{-int}dt = \frac{1}{2\pi}e^{i(m - n)t}dt\]
If \(m = n\), we have \(\frac{1}{2\pi}\frac{0}{2\pi}dt = 1\), otherwise, we have
\[\left.\frac{1}{2\pi}\frac{1}{i(m - n)}e^{i(m - n)t}\right|_0^{2\pi} = 0\]
So this set is indeed orthonormal, giving rise to the theory of Fourier series and the Fourier transform.
\end{document}