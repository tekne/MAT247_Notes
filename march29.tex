\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT247 Notes: Jordan Canonical Form}
\author{Jad Elkhaleq Ghalayini}
\date{March 29 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{cancel}

\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{corollary}{Corollary}
\newtheorem*{fact}{Fact}
\newtheorem*{note}{Note}
\newtheorem*{claim}{Claim}

\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}


\newcommand{\defeq}[0]{\vcentcolon=}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\nats}[0]{\mathbb{N}}

\usepackage{tikz}

\begin{document}

\maketitle

\begin{theorem}
Suppose \(V, \ip{}{}\) is an IPS over \(\reals\). Then if \((,)\) is another inner product on \(V\), there exists a basis \(\alpha\) for \(V\) such that
\begin{itemize}
    \item \(\alpha\) is orthonormal with respect to \(\ip{}{}\)
    \item \(\alpha\) is \underline{orthogonal} with respect to \((,)\)
\end{itemize}
\end{theorem}
\underline{This means}:
Let
\[S = \{x \in V | \ip{x}{x} = ||x|| = 1\}\]
\[E = \{x \in V | (x, x) = 1\}\]
Let \(\beta\) be \underline{any} orthonormal basis for \(V\).
Recall that \(V\) is isomorphic to \(\reals^n\), with
\[x \mapsto [x]_\beta = \begin{pmatrix} x_1 \\ \vdots \\ x_n \end{pmatrix}\]
such that
\[x_1\beta_1 + ... + x_n\beta_n = x\]
We have
\[\ip{x}{x} = x_1^2 + ... + x_n^2\]
\[\ip{x}{y} = x_1y_1 + ... + x_ny_n\]
So \(S\) is isomorphic to the \textit{\(n - 1\) sphere}
\[S^{n - 1} = \left\{\left.\begin{pmatrix} x_1 \\ \vdots \\ x_n\end{pmatrix} \in \reals^n\right| x_1^2 + ... + x_n^2\right\}\]
and \(E\) is ismorphic to an ``ellipsoid''. Since the \(\beta\)'s are perpendicular to each other, we in fact get
\[E \approx \left\{\left.\begin{pmatrix} x_1 \\ \vdots \\ x_n\end{pmatrix} \in \reals^n\right| a_1x_1^2 + ... + a_nx_n^2\right\}\]
where \(a_j = (\beta_j, \beta_j)\).
So for \(n = 2\), we get that \(S\) is the 1-sphere, whereas \(E\) is given by
\[a_1x_1^2 + a_2x_2^2 = 1\]
Since \(a_1, a_2\) are positive, we clearly get an ellipse.

Let's finally get a proof now
\begin{proof}
Take \(\beta\) to be an orthonomal basis (for \(\ip{}{}\)) for \(V\). Let the matrix \(A\) be defined
\[A_{ij} = (\beta_i,\beta_j)\]
(note for \(\ip{}{}\), this would give the identity matrix). We know
\[(\beta_i,\beta_j) = (\beta_j,\beta_i) \implies A_{ij} = A_{ji} \implies A^T = A^* = A\]
Since \(A\) is self adjoint, there is an orthonormal basis of eigenvectors for \(A\), which also means that \(C^{-1}AC\) is diagonal, where \(C\) is the orthonormal basis (all with respect to the sharp brackets!). Notice that
\[C^*C = I\]
Claim: \(\alpha = C\beta\).
Check: Defining \(\ip{\alpha}{\alpha}_{ij} = (\ip{\alpha_i}{\alpha_j})\).
\begin{lemma}
\[\ip{\beta C}{\beta C} = C^T\ip{\beta}{\beta}C\]
\end{lemma}
Let's assume this lemma is checked, and finish the proof. I'll then leave checking the lemma as an exercise.

So we have
\[\ip{\alpha}{\alpha} = \ip{\beta C}{\beta C} = C^T\ip{\beta}{\beta}C = C^TC = I\]
implying that \(\alpha\) is an orthonormal basis. In other words, if we taken an orthonormal basis and multiply it with an orthonormal matrix, we get another orthonormal basis.
Since \(CAC = D\), we have that \(\alpha\) is orthogonal w.r.t. \((,)\) as well.
\end{proof}

\section{Some group theory}
Suppose \(V\) is a finite dimensional \underline{vector space} over some field \(F\). Consider
\[\mathcal{L}(v) = \{T: V \to V: \text{ ``}T\text{ is linear''}\} = End(V)\]
Consider now the set of \underline{invertible} linear transformations, i.e. \(\det(T) \neq 0\). This is called \(GL(V)\). This set has various intersting properties:
\begin{itemize}

    \item [Composition]: If \(S, T \in GL(v)\), then \(S \circ T \in GL(v)\)

    \item [Associativity] (of composition): If \(R, S, T \in GL(v)\), then \((R \circ T) \circ T = R \circ (S \circ T)\)
    
    \item [Identity] (for composition): \(I \in GL(V)\), and if \(S \in GL(V)\),  \(I \circ S = S \circ I = S\)
    
    \item [Inverses] If \(T \in GL(V)\), there is a transformation \(T^{-1} \in GL(V)\) such that \(T \circ T^{-1} = T^{-1} \circ T = I\)

\end{itemize}

\begin{definition}[Group]
A group is a set \((G, \circ)\) with a composition \(g_1, g_2 \in G \implies g_1 \circ g_2 \in G\) such that
\begin{itemize}
    \item \(\circ\) is associative
    \item \(\exists e \in G, \forall g \in G, g \circ e = e \circ g = g\)
    \item \(\forall g \in G, \exists g^{-1} \in G\) s.t. \(g^{-1} \circ g = e  = g \circ g^{-1}\)
\end{itemize}
\end{definition}

One thing that's amazing about this definition is that we're not putting in very much, just a few axioms, and yet the theory of groups is incredibly rich. Almost nothing is required axiomatically, and yet it has huge consequences. As an example, \((GL(V), \circ)\) is a group. Other examples include
\begin{itemize}
    \item \(V = F^n\), \(GL(V) = GL_n(F) \subset M_n(F)\)
    \item \(G = (\{e\}, \circ)\)
    \item \underline{not} \(\varnothing\)
\end{itemize}
\subsection{Elementary Facts}
\begin{enumerate}[label=(\roman*)]

    \item \(e\) is \underline{unique}
    \begin{proof}
    Suppose \(e_1, e_2\) are identities. Then
    \[e_2 = e_1 \circ e_2 = e_1\]
    \end{proof}
    
    \item \(\forall g \in G, g^{-1}\) is unique.
    \begin{proof}
    Suppose \(g_1^{-1}, g_2^{-1}\) are inverses for \(g\). Then 
    \[g_1^{-1}gg_2^{-1} = eg_2^{-1} = g_1^{-1}e \implies g_2^{-1} = g_1^{-1}\]
    \end{proof}
    
    \item Cancellation:
    \[g \circ g_1 = g \circ g_2 \implies g^{-1}gg_1 = g^{-1}gg_2 \implies g_1 = g_2\]

\end{enumerate}
\begin{definition}
A \underline{subgroup} of \((G, \circ)\) is a subset \(H \subset G\) such that 
\begin{itemize}
    \item \(H \neq \varnothing\) 
    \item \(H\) is closed under \(\circ\) i.e. \(h_1, h_2 \in H \implies h_1 \circ h_2 \in H\)
    \item \(h \in H \implies h^{-1} \in H\)
\end{itemize}
\end{definition}
Fact: \((H, \circ)\) is a \underline{group}.

Some examples
\begin{itemize}

    \item Consider
    \[H = \{g \in GL(V) | \det(g) = 1\}\]
    We have, for \(h_1, h_2 \in H\)
    \[\det(h_1h_2) = \det(h_1)\det(h_2) = 1 \implies h_1 \circ h_2 \in H\]
    \[\det(h_1^{-1}) = 1/\det(h_1) = 1 \implies h_1^{-1} \in H\]
    Hence, since \(H\) is obviously nonempty, it is a subgroup of \(GL(v)\). This is called the special linear group \(SL(V)\), which is isomorphic to \(SL_n(F)\)
    
    \item Consider
    \[B = \{y \in GL_n(F) | g \text{ is upper triangular}\}\]
    Clearly, again, \(B\) is closed under composition (matrix multiplication) and inversion. This is called a Borel subgroup.

    \item Consider
    \[U(n) = \{g \in M_n(\comps) | g^*g = I_n\}\]
    \(U(n) \subset GL_n(\comps)\). Letting \(g \in U(n)\), \(g^{-1} = g^*, (g^*)^* = g \implies g^{-1} \in U(n)\). Similarly,
    \[g_1, g_2 \in U \implies (g_1g_2)(g_1g_2)^* = g_1g_2g_2^*g_1^* = g_1g_1^* = I \implies g_1 \circ g_2 \in U(n)\]
    So \(U(n)\) is a subgroup, the \underline{unitary group}.
    
    \item Similarly,
    \[O(n) = \{g \in M_n(\reals) | g^*g = I_n\} \subset GL_n(\reals)\]
    \[SO(n) = \{g \in O(n) | \det(g) = 1\} \subset SL_n(\reals)\]
    are groups. \(SO(2)\) and \(SO(3)\) are especially important in physics. \(SU(n)\) is analogously defined.

\end{itemize}
One construction that helps you really dig in and understand a group is the following: take \(G\) to be a group, and map \(G \to G\) by, for some element \(g \in G\), taking \(x \mapsto gxg^{-1}\). This is called \underline{conjugation by \(g\)}.

An Abelian group is one in which \(\forall g, h, g \circ h = h \circ g \iff gxg^{-1} = xgg^{-1} = x\). So conjugation by \(g\) is pretty boring in the Abelian case, but is very important forother groups.

Say \(x ~ y \iff \exists g \in G, y = gxg^{-1}\). For example, if \(A, B \in GL_n(F)\), \(A ~ B\) if \(B = CAC^{-1}\), i.e. \underline{similar} \(\iff\) \underline{conjugate}.

We want to claim that this notion of conjugation gives us a way to, in some sense, take apart a group. This I can't explain, it takes months and months, but I can give an example:

Let \(G = U(n)\). What we're interested in are: what are the conjugacy classes. We see that conjugacy is an equivalence relation: \(x ~ y \implies y ~ x\). So conjugacy breaks your group up into classes. What do we know? Suppose we have a matrix \(A \in U(n)\), i.e.
\[A^*A = AA^* = I \iff A^* = A^{-1}\]
So this tells us that \(A\) is \underline{normal}. And hence there exists an orthonormal basis of eigenvectors, i.e. \(\exists C \in Mat_{n \times n}(C)\) such that the columns of \(C\) are an orthonormal basis of \(\comps^n\) and \(C^{-1}AC = D\) is diagonal. Furthermore, since \(C\) is orthonormal,
\[C^*C = I \implies C \in U(n)\]
So this diagonal matrix \(D\) is conjugate to \(A\). And \(D \in U(n)\), since it is equal to \(C^{-1}AC\), where all three are in the unitary group. So \(D^*D = I\), implying that for each eigenvalue \(\lambda_i\), \(|\lambda_i|^2 = 1\).

In other words, we find that every conjugacy class contains a diagonal matrix. Furthermore, all we can do by changing the order of the eigenvectors is to permute the \(\lambda\)'s on the diagonals around. Hence, the conjugacy classes in \(U(n)\) are the same as
\[\{\lambda_1,...,\lambda_n \in \comps | |\lambda_j|^2 = 1\}\]
up to permutation.

\end{document}