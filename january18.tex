\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT247 Notes: Diagonalizability and Applications}
\author{Jad Elkhaleq Ghalayini}
\date{January 18 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}

\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{corollary}{Corollary}
\newtheorem*{fact}{Fact}
\newtheorem*{note}{Note}

\usepackage{tikz}

\begin{document}

\maketitle

\section{Applications of Diagonalization: Differential Equations}
Suppose we're doing a calculus thing, and
\[f'(t) = \lambda f(t) \iff f(t) = ce^{\lambda t} \text{ for some constant } c \in \mathbb{C}\]
Suppose instead of having one function like this, we have two functions,
\[f_1'(t) = \lambda_1f_1(t), f_2'(t) = \lambda_2f_2(t) \implies f_1(t) = c_1e^{\lambda_1t}, f_2(t) = c_2e^{\lambda_2t}\]
Now imagine we have a more general situation, for example,
\[f_1'(t) = af_1(t) + bf_2(t), f_2'(t) = cf_1(t) + df_2(t)\]
Using linear algebra, this can be as easy as in the first case. We can rewrite this by defining \(f(t) = \begin{pmatrix} f_1(t) & f_2(t) \end{pmatrix}^T\), and, with \(f'(t) = \begin{pmatrix} f_1'(t) & f_2'(t) \end{pmatrix}^T\), we get, for the first equation above,
\[f'(t) = \begin{pmatrix} \lambda_1 & 0 \\ 0 & \lambda_2 \end{pmatrix}f(t) \implies f(t) = \begin{pmatrix}c_1 e^{\lambda_1t} \\ c_2e^{\lambda_2t}\end{pmatrix}\]
We can separate this out, writing
\[f(t) = c_1e^{\lambda_1t}e_1 + c_2e^{\lambda_2t}e_2\]
Now consider the more complicated system
\[A = \begin{pmatrix} -1 & 3/2 \\ 6 & -1 \end{pmatrix}, f'(t) = Af(t)\]
We can immediately guess the answer: constants, times the eigenvectors of \(A\), times \(e\) to the power of the corresponding eigenvalues times t.

Recall, from early on in the course, 
\[Q = \begin{pmatrix} 1 & 1 \\ 2 & -2 \end{pmatrix} \implies D = Q^{-1}AQ = \begin{pmatrix} 2 & 0 \\ 0 & -4 \end{pmatrix}\]
Write: \(f(t) = Qg(t), g(t) = \begin{pmatrix}g_1(t) & g_2(t)\end{pmatrix}^T\)
\[Q^{-1}f(t) = g(t), \ \ f'(t) = Qg'(t)\]
So
\[Qg'(t) = AQg(t) \implies g'(t) = Q^{-1}AQg(t) = Dg(t) \implies g(t) = \begin{pmatrix} c_1e^{2t} \\ c_2e^{-4t} \end{pmatrix}\]
\[\implies f(t) = Qg(t) = c_1e^{2t}\begin{pmatrix} 1 \\ 2 \end{pmatrix} + Q_2e^{-4t}\begin{pmatrix} 1 \\ -2 \end{pmatrix}\]
So if \(A\) is diagonalizable, having \(n\) linearly independent eigenvectors \(v_1,...,v_n\) with eigenvalues \(\lambda_1,...,\lambda_n\) respectively, we get
\[f(t) = \sum_{k = 1}^nc_ke^{\lambda_kt}v_k \text{ for some constants } c_1,...,c_n\]
Back to our example, in differential equations, we heavily use initial conditions. So at time zero, where are we?
\[f(0) = c_1\begin{pmatrix}1 \\ 2\end{pmatrix} + c_2\begin{pmatrix}1 \\ -2\end{pmatrix}\]
So we see \(c_1, c_2\) give us our initial position. At every point in the plane, there's a unique trajectory going through in time. Suppose, however, we had a point on the eigenline given by the first eigenvalue. Then, we have \(c_1 = c\), \(c_2 = 0\). So we're just racing along the eigenline with exponential velocity! What happens, however, if we start off the eigenline? We'd start tracking the line, but as we get closer, slowly approach the second eigenline and eventually, it would cause us to curve out.

Now remember what we said about this being just as easy as the trivial case? Well, we get solution
\[f'(t) = Af(t) \implies f(t) = e^{At}t\]
But what is \(e^{A}\) for some matrix \(A\)? We know
\[e^t = 1 + t + \frac{t^2}{2!} + \frac{t^3}{3!} + ...\]
So it should be about the same for matrices
\[e^A = 1 + A + \frac{A^2}{2!} + \frac{A^3}{3!} + ...\]
This is convergent for any \(A\). We can put a \(t\) in here to get
\[e^{At} = 1 + At + \frac{A^2t^2}{2!} + ...\]
We get
\[\frac{d}{dt}(e^{At}) = A + \frac{A^2t} + \frac{A^3t^2}{3!} ... = Ae^{At}\]
So
\[F(t) = e^{At} \implies F'(t) = Ae^{At}\]
Note that \(F(t)\) is valued in \(n \times n\) matrices. But we wanted a solution which was a vector valued function! All we have to do is let
\[f(t) = F(t)c, c \in \mathbb{R}^n\]
This gives us a solution which depends on the choice of \(c\), and for any \(c\) it works!
Back to our original solution, we note that, for the \(n\)-equation case:
\[f(t) = \begin{pmatrix} e^{\lambda_1t} & ... & 0 \\ \vdots & \ddots & \vdots \\ 0 & ... & e^{\lambda_nt}\end{pmatrix}\begin{pmatrix} c_1 \\ \vdots \\ c_n \end{pmatrix}\]

\section{Cayley-Hamilton Theorem}

\begin{theorem}
Let \(T: V \to V\) be a linear transformation, \(\dim V = n < \infty\), 
\[f(t) = P_T(t) = \det([T]_\beta - tI_n) = (-1)^n\left(t^n + \sum_{k = 0}^{n - 1}{a_kt^k}\right)\]
Then
\[f(T) = (-1)^n\left(T^n + \sum_{k = 0}^{n - 1}{a_kT^k}\right) = 0_V\]
\end{theorem}
What can confuse people about this theorem is that I'm plugging a linear transformation into a polynomial. There's a matrix version that's somewhat easier to think about: suppose we have an \(n \times n\) matrix \(A\), and its characteristic polynomial \(f(t) = P_A(t)\). Then
\[f(A) = (-1)^n\left(A^n + \sum_{k = 0}^{n - 1}{a_kA^k}\right) = 0\]
Example:
\[A = \begin{pmatrix} 0 & 1 \\ -1 & 0 \end{pmatrix} \implies P_A(t) = t^2 + 1 = t^2 + t^0\]
\[A^2 = \begin{pmatrix} -1 & 0 \\ 0 & - 1 \end{pmatrix}\]
So
\[f(A) = A^2 + I_2 = 0\]

\vspace{5mm}

\begin{definition}
Let \(T: V \to V\) for linear, and \(V\) be \underline{any} vector space. A \(T\)-invariant subspace \(W\) is a subspace \(W \subseteq V\) such that
\[T(W) \subseteq W\]
\end{definition}
Examples:
\begin{itemize}
    \item \(W = \{0\}\)
    \item \(W = V\)
    \item \(W = N(T)\)
    \item Let \(b\) be an eigenvalue of \(T\). \(W = \{c \cdot b: b \in F\}\)
    \item \(W = E_\lambda(T)\) for some eigenvalue \(\lambda\) of \(T\)
\end{itemize}
Method of generating such \(W\)'s:
Take \(v \in V\). Consider what happens when we take
\[span\{v, T(v), T^2(v), T^3(v),...\}\]
\begin{enumerate}
    \item \(W\) is \(T\)-invariant,since if
    \[w = \sum_ia_iT^i(v), T(w) = \sum_ia_iT^{i + 1}(v)\]
    \item \(W\) is the smallest \(T\)-invariant subspace containing \(v\).
\end{enumerate}
\begin{definition}
\[<v>_T = span\{v, T(v), T^2(v), T^3(v),...\}\]
be the cyclic subspace generated by \(v\)
\end{definition}
\begin{definition}
Let \(T: V \to V\) be linear, and \(W \subseteq V\) be a T-invariant subspace. Define \(T_W: W \to W, w \mapsto T(v)\), i.e. \(T_W\) is the \underline{restriction} of \(T\) to \(W\).
\end{definition}
\begin{theorem}
Suppose \(\dim V = n < \infty\). Then
\[P_{T_W}(T)|P_T(t)\]
\end{theorem}
\begin{proof}
Choose a basis \(\beta_W = \{v_1,...,v_p\}\) for \(W\). Complete to a basis \(\beta: \{v_{11},...,v_{p1},v_{p + 1},...,v_n\}\)
\[[T]_\beta = \begin{pmatrix}[T_W]_{\beta_W} & B \\ 0 & C \end{pmatrix}\]
We hence have
\[P_T(t) = \det([T]_\beta - t \cdot I_n) = \begin{vmatrix} [T_W]_{\beta_W} - t \cdot I_p & B \\ 0 & C - t \cdot I_{n - p} \end{vmatrix}\]\[ = \det([T_W]_{\beta_W})\det(C - t \cdot I_{n - p}) = P_{T_W} \cdot \det(C - t \cdot I_{n - p})\] 
\end{proof}
\begin{theorem}
Let \(T: V \to V, \dim V = n < \infty\), \(W = <v>_T, \dim W = p\).
Then
\begin{enumerate}[label=(\alph*)]
    \item \(\{v, T(v), ..., T^{p - 1}(v)\}\) is a basis for \(W\)
    \item Write \[T^p(v) + a_{p - 1}T^{p_1}(v) + ... + a_1T(v) +a_0v = 0\]
    Then
    \[P_{T_W}(t) = (-1)^p(t^p + a_{p-1}t^{p - 1} + ... + a_1t + a_0)\]
\end{enumerate}
\end{theorem}
\begin{proof}
\begin{enumerate}[label = (\alph*)]

    \item Consider \[\{v\}, \{v, T(v)\}, \{v, T(v), T^2(v)\},...,\{v, T(v), T^2(v),...,T^{j - 1}(v)\}\]
    Sooner or later, there's going to be one that's independent, and one that's dependent afterwards, since \(v\) itself is \(\dim V = n < \infty\). Take \(j\) to be the maximum number such that the above collection of sets are all linearly independent.
    
    Note: \(j \leq p\). Let
    \[U = span\{v,T(v),...,T^{j - 1}(v)\}\]
    Claim: \(U\) is \(T\)-invariant. Take \[u = c_0v + c_1T(v) + ... + c_{j - 1}T^{j - 1}(v)\]
    \[T(u) = c_0T(v) + c_1T^2(v) + ... + c_{j - 1}T^j(v)\]
    We know that the set
    \[\{v, T(v), T^2(v),...,T^j(v)\}\]
    is linearly independent. So we can write
    \[T^j(v) = \sum_{k = 0}^{j - 1}a_kT^k(v) \implies T(u) = \sum_{k = 0}^{j - 2}c_kT^{k + 1}(v) + c_{j - 1}\sum_{k = 0}^{j - 1}a_kT^k(v) \in U\]
    We know \(W\) is the smallest \(T\)-invariant subspace, so \(U \subseteq W\). But \(\dim U = j \leq p = \dim W \implies W \subseteq U\). So \(U = W\), therefore
    \[\{v, T(v), T^2(v),...,T^j(v)\}\]
    is a basis for \(W\)
    
    \item Let \(\beta_W = \{v, T(v), ..., T^{p - 1}(v)\}\), with \(T^p(v) = \sum_{k = 0}^{p - 1}a_kT^k(v)\)
    \[P_{T_W}(t) = \det([T_W]_{\beta_W} - tI_p) = 
    \det\left(\begin{pmatrix} 
    0 & 0 & ... & 0 & -a_0 \\
    1 & 0 & ... & 0 & -a_1 \\
    0 & 1 & ... & 0 & -a_2 \\
    \vdots & \vdots & \ddots & \vdots & \vdots \\
    0 & 0 & ... & 1 & -a_{p - 1}
    \end{pmatrix} - tI_p\right)\]
    Homework: prove this gives the correct characteristic polynomial (this is called the \textit{companion matrix} to the desired polynomial)
\end{enumerate}
\end{proof}
\end{document}