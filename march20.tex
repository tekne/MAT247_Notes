\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT247 Notes: Jordan Canonical Form}
\author{Jad Elkhaleq Ghalayini}
\date{March 20 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{cancel}

\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{corollary}{Corollary}
\newtheorem*{fact}{Fact}
\newtheorem*{note}{Note}
\newtheorem*{claim}{Claim}

\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}


\newcommand{\defeq}[0]{\vcentcolon=}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\nats}[0]{\mathbb{N}}

\usepackage{tikz}

\begin{document}

\maketitle

Recall
\[V = W_1 \oplus W_2\]
if \(V = W_1 + W_2\)  and \(W_1 \cap W_2 = \{0\}\)
i.e. \[\forall x \in V, \exists! x_1, x_2 \in W_1, W_2, x = x_1 + x_2\]
\begin{definition} 
The linear transformation \(T: V \to V\)
\[T(x) = x_1\]
is the \underline{projection of \(V\) to \(W_1\) along \(W_2\)}
\end{definition}
Examples: \(V = \reals^2\)
\begin{enumerate}
    
    \item Let \[W_1 = \reals \cdot e_1, W_2 = \reals \cdot e_2\]\
    giving
    \[T\begin{pmatrix} a_1 \\ a_2 \end{pmatrix} = \begin{pmatrix} a_1 \\ 0 \end{pmatrix}\]
    
    \item Let \[W_1 = \reals \cdot e_1, W_2 = \reals \cdot (e_1 + e_2)\]
    Clearly,
    \[\begin{pmatrix} a_1 \\ a_2 \end{pmatrix} = (a_1 - a_2)\begin{pmatrix} 1 \\ 0 \end{pmatrix} + a_2\begin{pmatrix} 1 \\ 1 \end{pmatrix}\]
    implying
    \[T\begin{pmatrix} a_1 \\ a_2 \end{pmatrix} = \begin{pmatrix} a_1 - a_2 \\ 0 \end{pmatrix}\]

\end{enumerate}
The point of this is that projections depend not only on the target, but also on the other subspace.

If \(\exists W_1, W_2\) which gives you \(T\) in this way, we call \(T\) a projection. We can easily find out what \(W_1, W_2\) are by noticing that
\[N(T) = W_2, R(T) = W_1\]
Proposition: \(T: V \to V\) is a projection \(\iff T^2 = T\)
\begin{proof}
We start with the easier direction:
\begin{itemize}
    \item [``\(\implies\)''] Let \(x \in V, x_1 \in W_1\) such that \(T(x) = x_1\). We have
    \[T^2(x) = T(T(x)) = T(x_1) = x_1 = T(x)\]\[\implies T(x) = T^2(x) \forall x \in V \iff T = T^2\]
    
    \item [``\(\Longleftarrow\)''] Suppose \(T^2 = T\). Let \(W_1 = R(T), W_2 = N(T)\). Claim: \(V = W_1 \oplus W_2\).
    \begin{enumerate}
        
        \item \(T^2 = T \implies T(I - T) = 0 \implies N(T) = R(I - T)\)
        \[\implies R(1 - T) \subseteq N(T)\]
        \[x \in N(T) \implies (I - T)(x) = x \implies x \in R(1 - T)\]
        \[\implies R(1 - T) \supseteq N(T) \implies N(T) = R(1 - T)\]
        
        \item \(I = T + I - T \implies x = T(x) + (I - T)(x) \implies V = W_1 + W_2\) since every vector can be written as something in the range of \(T\) and something in the range of \(I - T\), which is equal to the nullspace of \(T\). 
        
        \item Let \(T(x) \in R(T) \cap N(T) = R(T) \cap R(T - I) \implies T(T(x)) = T(x) = 0\)
        \[\implies R(T) \cap N(T) = \{0\}\]
        
        \item If \(x = x_1 + x_2, x_1 \in R(T), x_2 \in N(T)\), then, since \(x_1 \in R(T) \implies x_1 = T(y)\) for some \(y \in V\),
        \[T(x) = T(x_1) + \cancel{T(x_2)} = T^2(y) = T(y) = x_1\]
        
    \end{enumerate}
    
\end{itemize}
\end{proof}
Suppose \(V = \ip{}{}\) is an IPS,
\begin{definition}
\(T: V \to V\) is an \underline{orthogonal} projection if
\begin{enumerate}
    \item \(T\) is a projection
    \item (a) \(N(T) = R(T)^\perp\), (b) \(R(T) = N(T)^\perp\)
\end{enumerate}
Note that we did \underline{not} say finite dimensional, which is why we have to be careful here. If \(V\) is finite dimensional, (2a) \(\iff\) (2b).
\end{definition}
Suppose \(W \subset V = \text{ IPS }, \dim W < \infty\).
Let \(w_1,...,w_m\) be an orthonormal basis for \(W\), \(T: V \to V\),
\[x \mapsto \ip{x}{w_1}w_1 + ... + \ip{x}{w_m}w_m = y\]
Proposition: \(T\) is an orthogonal projection
\begin{proof}
\begin{itemize}
    \item \(T^2 = T\): 
    \[T(T(x)) = T(y) = \ip{y}{w_1}w_1 + ... + \ip{y}{w_m}w_m\]
    \[ = \ip{x}{w_1}w_1 + ... + \ip{x}{w_m}w_m = T(x)\]
    \item \[\ip{x - T(x)}{w} = \ip{x}{w_j} - \ip{x}{w_j} = 0\]
    \[\implies x - T(x) \in W^\perp\]
    \[N(T) = R(I - T), N(T) \subseteq R(T)^\perp = W^\perp\]
    If \(x \in W^\perp\), \(x \in N(T) \implies N(T) = R(T)^\perp\).
\end{itemize}
\end{proof}
\end{document}